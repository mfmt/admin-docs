+++
date = 2020-02-06T13:53:34Z
title = "Install Debian on a physical host machine"
categories = ["install"]
+++

# Server Installation

## Initial steps

 * Add the server to the hosts.yml file and create a host_vars file. Ensure
   there is a console entry and then run ansible to the console server (in
   Telehouse it's `clr`). Read the top of the [IMPI](/ipmi) page as well to get
   the settings right.
 * Ensure that [network boot](/network-boot) is setup in the cabinet in which
   you are installing the server.
 * Plug in monitor and keyboard
 * Enter Bios/Setup. Specify that Bios should output to serial console.
 * Ensure that server skips errors like no keyboard attached
 * If it's IMPI, configure bios to use [IPMI](/ipmi).
 * Reboot and select Boot Options and choose to boot to network from F12 menu
 * NOTE: If it's IPMI (most new servers), you won't be able to access the debirf image via the SOL interface unless you fix the console settings - by default, the debirf image will try to boot with console=ttyS0, but IPMI will be listening on ttyS1, so you have to adjust it:
   * Connect via iKVM via the web *first* 
   * At the grub prompt, edit the debirf line.
   * Change to: `console=ttyS1,115200n8` (change ttyS0 to ttyS1)

## Drive partioning/Disk setup

### The big picture is...

  * *All partitions should be Primary*

  * One tiny partition used for bios/grub data so we can boot from them.
  * A second approximately 1024MB partition on both disks: configured as RAID1, used as boot partition
  * The rest of the space on each disk: configured as RAID1, used as encrypted disk
  * Encrypted disk: used as physical volume for LVM
  * Create volume group for the SATA disks (vg0) and one for the SSD disks (vg1)
  * Create standard partitions as logical volumes

### Details - Install manually via debootstrap

#### Partitioning 

Check disk's sector size! If it's 512B per sector, these are good numbers. If it's 4096 they chould be changed.

Also: note to self - we should increase spare capacity at end.

```
for disk in a b c d; do
  parted /dev/sd${disk} -- mklabel gpt
  parted /dev/sd${disk} -- unit s mkpart biosboot 8192 16383 
  parted /dev/sd${disk} -- set 1 bios_grub on 
  parted /dev/sd${disk} -- unit s mkpart boot 16384 1015807
  parted /dev/sd${disk} -- set 2 raid on 
  parted /dev/sd${disk} -- unit s mkpart pv 1015808 -196608 
  parted /dev/sd${disk} -- set 3 raid on 
done
```

#### RAID

```
mdadm --create --raid-devices=4 --level=1 --metadata=1.0 --verbose /dev/md0 /dev/sd[abcd]2
mdadm --create --raid-devices=2 --level=1 --metadata=1.0 --verbose /dev/md1 /dev/sd[ab]3
mdadm --create --raid-devices=2 --level=1 --metadata=1.0 --verbose /dev/md2 /dev/sd[cd]3
```

#### Cryptsetup

```
cryptsetup luksFormat /dev/md1
cryptsetup luksOpen /dev/md1 md1_crypt
cryptsetup luksFormat /dev/md2
cryptsetup luksOpen /dev/md2 md2_crypt

```

#### lvm

```
pvcreate /dev/mapper/md1_crypt
vgcreate vg0 /dev/mapper/md1_crypt
lvcreate --name swap --size 1GB vg0
lvcreate --name root --size 20GB vg0
lvcreate --name var --size 10GB vg0
lvcreate --name tmp --size 1GB vg0
```

#### Filesystems

Then, create fileystems:

 * For the boot partition `mkfs -t ext4 /dev/md0`
 * Repeat for each logical volume
```
for part in var tmp root; do
  mkfs -t ext4 /dev/mapper/vg0-${part}
done
mkswap /dev/mapper/vg0-swap
```

#### Install and run debootstrap

Before you can install Debian, mount all partitions in /mnt

```
mount /dev/mapper/vg0-root /mnt
mkdir -p /mnt/{boot,var,proc,dev,sys,tmp,run/udev}
mount /dev/md0 /mnt/boot
mount /dev/mapper/vg0-var /mnt/var
mount /dev/mapper/vg0-tmp /mnt/tmp
```

Note: Don't mount /proc, /sys, and /dev before running debootstrap - you will
end up with a broken installation (apt won't be available for one thing).

Start networking:

 * If DHCP is available, simply type: `systemctl start systemd-networkd`
 * Otherwise, manually add an IP address and route, e.g.:
 ```
 ip addr add dev eno1 37.218.247.161/28
 ip link set up dev eno1
 ip route add default via 37.218.247.174 dev eno1
 echo nameserver 4.2.2.1 > /etc/resolv.conf
 ```

Update and install debootstrap: `apt update && apt install debootstrap`

Run debootstrap: `debootstrap buster /mnt`

#### Chroot

Bind mount kernel directories:

```
mount -o bind /proc /mnt/proc
mount -o bind /sys /mnt/sys
mount -o bind /dev /mnt/dev
# See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=918590
mount -o bind /run/udev /mnt/run/udev
```

Enter a chroot environment: `chroot /mnt`

#### Prep work

Set proper permissions on /tmp: `chmod 1777 /tmp`

Set some configuration files that should be in place before packages are installed.

 * /etc/fstab
```
printf "UUID=$(blkid --output value /dev/mapper/vg0-root | head -n1)  / ext4  errors=remount-ro 0 1
UUID=$(blkid --output value /dev/md0 | head -n1)  /boot ext4  defaults  0 2
UUID=$(blkid --output value /dev/mapper/vg0-var | head -n1) /var  ext4  defaults  0 2
UUID=$(blkid --output value /dev/mapper/vg0-tmp | head -n1) /tmp  ext4  defaults  0 2
/dev/mapper/vg0-swap  none  swap  sw  0 0
" > /etc/fstab

```
 * /etc/crypttab

```
printf "# <target name> <source device> <key file>  <options>\n" > /etc/crypttab
for md in $(ls /dev/mapper/*_crypt| egrep -o 'md[0-9]+'); do printf "${md}_crypt UUID=$(blkid --output value /dev/${md}|head -n1)  none  luks\n" >> /etc/crypttab; done
```

#### Install packages

Install a lot of necessary packages for booting: `apt install mdadm lvm2 cryptsetup grub-pc linux-image-amd64 bridge-utils openssh-server`

#### Update files required for booting

  * /etc/systemd/network/br0.netdev
  ```
[NetDev]
Name=br0
Kind=bridge
  ```
  * /etc/systemd/network/br0.network
  ```
[Match]
Name=br0

[Network]
Address=xx.xx.xx.xx/xx
Gateway=xx.xx.xx.x
  ```
  * /etc/systemd/network/cable0.network
  ```
[Match]
Name=eno1

[Network]
Bridge=br0
   ```
  * /etc/default/grub
  ```
# Add the following (ttyS0 instead if not using IPMI)
GRUB_CMDLINE_LINUX="console=ttyS1,115200n8"
# And
GRUB_TERMINAL=serial
GRUB_SERIAL_COMMAND="serial --unit=0 --speed=115200 --word=8 --parity=no --stop=1"
  ```
  * /etc/hosts
  ```
127.0.0.1	localhost
xx.xx.xx.xx	yyy.mayfirst.org yyy 
::1		localhost ip6-localhost ip6-loopback
ff02::1		ip6-allnodes
ff02::2		ip6-allrouters
  ```
  * /etc/hostname
  ```
justnamenotdomain
  ```

#### Almost done!

 * Enable networking: `systemctl enable systemd-networkd`
 * Generate /etc/mdadm/mdadm.conf file. Generate with: `/usr/share/mdadm/mkconf > /etc/mdadm/mdadm.conf`
 * Set root passwd: `passwd`
 * Add your ssh public key to `/root/.ssh/authorized_keys`
 * Note the ssh fingerprints: `for key in $(ls /etc/ssh/ssh_host_*.pub); do ssh-keygen -l -f "$key"; done`
 * Create a DNS record from control panel.
 * Ensure latest changes are reflected: `update-grub && update-initramfs -u`
 * Reboot!

#### After reboot

Copy the file `roles/kvm/files/mf-initialize-server` to the server and run it.
It will install some basic packages and then ask for your control panel login.
With the login, it will create DNS ssh fingerprint records for the server.
