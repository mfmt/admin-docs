+++
date = 2020-10-29T13:53:34Z
title = "Elasticsearch Upgrades"
categories = ["logging" ]
+++

Elasticsearch has many different components that we use:

 * [elasticsearch](/elasticsearch): The server database itself
 * [journalbeat](/journalbeat) and [metricbeat](/metricbeat): which feed information from our servers to the databsae
 * [kibana](/kibana): the web interface for reviewing data

([elastalert](/elastalert) also deserves mention, but it is not included since
it is released independently of the rest.)

These four pieces of software are all released with the same version number at
the same time. Because they are so tightly integrated, it's a good idea to
upgrade all of them together.

To facilitate this process, the elasticsearch version number is included as a
variable in the hosts.yml file (`m_elastic_version`).

All servers use apt pinning to ensure we keep them at the right version.

And, when we change the version, ansible will automatically upgrade each
server. However, these packages don't trigger a restart after the upgrade,
giving us yet more control of exactly how to manage it.

## Steps to take when upgrading

1. Bump the version number in the `hosts.yml` file and push out to *just* the
   log server. Use `--tags elasticsearch_repos,elasticsearch,metricbeat,journalbeat`.
1. Restart `elasticsearch` on the log server and ensure there are no errors.
1. Run `mf-elastic-beat-setup` and `ELASTIC_PACKAGE=journalbeat
   mf-elastic-beat-setup`- this will rebuild the indices and ensure
   [ilm](/elasticsearch-index-lifetime-management) is setup properly for the
   new version. It deletes any existing indices for the current version so,
   it's important to run this step before upgrading journalbeat and metricbeat
   on the other servers, otherwise we lose some logs.
1. Restart metricbeat and journalbeat on the es server.
1. Push out via ansible to all servers (`--tags elasticsearch_repos,metricbeat,journalbeat,kibana
`).
1. Restart journalbeat and metribeat on all servers via: `ansible -i /path/to/hosts.yml
   'all:!external' -m shell -a 'systemctl restart metricbeat && systemctl
   restart journalbeat'`

