+++
date = 2024-07-13T13:53:34Z
title = "Trouble shoot KVM guest"
categories = ["install"]
weight = -50
+++

Sometimes KVM guests fails to start. To troubleshoot, we can boot into a grml live installation.

1. On the host, [download the small grml live install](https://grml.org/download/) into `/usr/local/share/ISOs`
2. As the kvm guest user, create a symlink: `ln -s /usr/local/share/ISOs/grml64-small_YYYY.MM.iso ~/vms/${USER}/cd.iso`
3. Reboot the KVM guest and quickly enter the screen session
4. Hit "Esc" key to get to boot menu
5. Choose the CD/DVD option
6. Under "Additional boot entires for grml64-small", choose "serial console" (at the bottom)

#### Chroot

The really get at the installation, you may need to mount the directories in
`/mnt` and use chroot to fix what is wrong.

A typical KVM guest will need the following - be sure to adjust as needed!

```
mount /dev/vda2 /mnt
mount /dev/vda1 /mnt/boot
mount /dev/vda4 /var
mount -o bind /proc /mnt/proc
mount -o bind /sys /mnt/sys
mount -o bind /dev /mnt/dev
```

The grml live install comes with zsh, which will cause an error if you try to `chroot` because we don't have zsh installed. So...

```
SHELL=/bin/bash chroot /mnt

```

At this point, you may want to run `grub-install /dev/vda` or `update-grub`.
