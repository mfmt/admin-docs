+++
date = 2020-07-06T13:53:34Z
title = "Nginx"
categories = ["web"]
+++

[Nginx](https://nginx.org) is a web server. It provides both both a front-end
proxy for other nginx or apache web servers in our network and also serves as a
standard web service for other services.

## Web proxy

You can configure a server to use an nginx proxy by adding a stanza to the
server's yml file. For example, in an ansible definition for a server
called report001.mayfirst.org in which you will be launching a web 
service listening on port 5601 via the url: https://report.mayfirst.org,
you might include the following snippet:

    nginx_proxy:
      hosts: [ "webproxy001.mayfirst.org" ]
      upstream:
        name: report001
        ip: 1.2.3.4 
        port: 5601
      sites:
        - server_names: report.mayfirst.org www.report.mayfirst.org
          canonical_domain: report.mayfirst.org
          tls_certificate_name: report.mayfirst.org
          upstream_name: report001

When ansible runs for the host webproxy001, it will pick up this definition and
build out the proper nginx configuration files to proxy all connections to the
host `report001.mayfirst.org`.

## Standard web service

TBD
