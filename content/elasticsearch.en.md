+++
date = 2019-08-13T13:53:34Z
title = "Elasticsearch setup"
categories = ["logging", "intrusion-detection"]
+++

[Elasticsearch](https://www.elastic.co/products/elasticsearch) (es) is is a
database designed to injest large amounts of data while providing an easy to
use query language to search it.

It's installed using es provided apt repos (not in Debian).

The es server(s) are dedicated to just *running* Elasticsearch. Other servers
and services work together with Elasticsearch, including:

 * [Journalbeat](/journalbeat) - all servers run
   [Journalbeat](https://www.elastic.co/guide/en/beats/journalbeat/current/index.html),
   which sends journald data to the es server.
 * [Metricbeat](/metricbeat) - all servers run
   [metricbeat](https://www.elastic.co/guide/en/beats/metricbeat/current/index.html),
   which sends server metrics data to the es server.
 * [Filebeat](/filebeat) - some servers run
   [filebeat](https://www.elastic.co/guide/en/beats/filebeat/current/index.html),
   which sends the contents of a given log file to the es server (e.g. nginx).
 * [Kibana](kibana) - provides a user interface for querying the es server via
   https://report.mayfirst.org/. 
 * [Simplemonitor](simplemonitor) - queries the data going into es and provides
   alerts when pre-defined conditions are met.

## User management

We use basic auth with pre-defined usernames and passwords to grant access to
the various programs that send and/or monitor our es server, including
`kibana_system`, `journalbeat_writer`, and `simplemonitor` users.

Their passwords are set in a [ansible vault](ansible-vault) file called
secrets.yml kept in the inventory.

## API helper

On the elasticsearch server, we have `mf-elastic-api` which is a helper for
sending commands to the serveri curl. It auto-populates the user credentials,
making it easier to experiment with the otherwise unweildy curl arguments.
