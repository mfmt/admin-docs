+++
date = 2019-11-08T13:53:34Z
title = "Simple Monitor"
categories = ["logging", "alerts"]
+++

[Simlemonitor](https://jamesoff.github.io/simplemonitor) is a python daemon
that monitors a set of remote services and sends alerts whenver it detects that
a remote service is no longer accessible.

Simplemonitor is entirely independent of [elastalert](/elastalert), which
provides all other alerts) to ensure we get alerts even if our
[elasticsearch](/elasticsearch) infrastructure is not accessible.

The set of services to monitor is automatically generated based on the
`simplemonitor` variable in the hosts.yaml [ansible](/ansible) file.

For example:

    simplemonitor:
      # The key will be used as the label to indentify the resource that is offline in the alert.
      server-name-ping:
        # Common types are 'host' (ping), 'http', or 'tcp'
        type: host
        host: domain.org
      server-name-http:
        type: http
        url: https://joe.was.org/
        # Path to certs to verify (might not be needed in production)
        verify_hostname: /etc/ssl/certs/ca-certificates.crt 
      server-name-smtp:
        type: tcp
        host: domain.org
        port: 25 

