+++
date = 2021-05-03T13:53:34Z
title = "Jitsi Meet Jibri Server"
categories = ["jitsimeet"]
+++

## Overview 

The [jitsimeet jibri server](https://github.com/jitsi/jibri) provides a
recording and broadcasting service.

We are currently only using the recording service.

Jibri should be installed on a separate machine from the jitsimeet server
because jibri can be a CPU hog. However, if you want a Jitsi Meet instance to
integrate with jibri, be sure to set `jitsimeet_jibri: true` in the jitsimeet
instance's yaml file.

Using the jibri debian package takes care of most of the aspects of installing
jibri with the exception of the `/etc/jitsi/jibri/jibri.conf` file, which (as
of May 2021) is considerably difficult to figure out since the reference file
has not examples for use with recordings.

## Taming ffmpeg

By default, jibri calls ffmpeg with settings that consume wild amounts of CPU.
To control CPU usage, we provide an ffmpeg wrapper that intercepts the ffmpeg
call made by jibri and changes the video scale. There are probably many more
optimizations we could make.

Note: when ffmpeg starts to fall behind because it does not have enough CPU, it
starts storing unprocessed frames in RAM. This will appear like a memory leak,
and can simply deplete RAM until the process is killed by the kernel.

While it appears that there is not enough RAM, the real resource depletion is
CPU and it should be addressed by providing a faster CPU or manipulating ffmpeg
to be less CPU intensive.

For more inf, see [this long thread](https://github.com/jitsi/jibri/issues/269).

# The extra pieces

Most of the work of our ansible instructions cover the extra pieces:

 * A finalize script that runs when a recording is complete to generate a webm
   version and notify us that the recording is complete.
 * An email template to be used when sending this email.
 * The html, css and img files used to display the video recordings
 * The systemd timer file that purges recordings older then 14 days.
