+++
date = 2023-11-11T13:53:34Z
title = "Setting up a new shared web server"
categories = ["web", "red", "control-panel"]
+++

The following steps should be taken to setup a new web server:

1. In ansible seed repo, edit `hosts.yml` to copy each line with `weboriring001` to
   your new name.
1. Execute `sower --live generate:weborigin --parent hostxxx.mayfirst.org` to generate a new weborigin yaml file.
1. Follow [regular kvm guest setup steps](/kvm-manager/#what-to-do-specifically)
1. Be sure you created the ssh key in the step above!
1. Grant access to the mysql server so your weborigin server can run proxysql properly. Adjust to account for all red enabled mysql servers. And, you may need to manually copy the key to pachamama if it is still in use.
   `sower --live playbook --tags proxysql mysql00[4567].mayfirst.org`
1. Add as upstream servers to web proxies:
   `sower --live playbook --tags nginx-proxy webproxy00[12345].mayfirst.org multi001.mayfirst.org`
1. Add IP to mail filter servers so it can relay email:
   `sower --live playbook --tags postfix mailfilter00[12].mayfirst.org`
1. Insert the server into the `red_server` table:
   `INSERT INTO red_server SET accepting = 0, server = 'weborigin00x.mayfirst.org', closed = 1, mountpoint = 'data0xxx', server_default_php_version = '8.1', server_proxysql = 1, server_dedicated_mail = 0;`
