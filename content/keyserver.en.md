+++
date = 2020-01-17T13:53:34Z
title = "Keyserver"
categories = ["x509"]
+++

## Overview 

May First relies on [public key infrastructure
(PKI)](https://en.wikipedia.org/wiki/Public_key_infrastructure) throughout the
network to ensure encrypted and authenticated communications between the public
and our servers and between our servers themselves.

In most cases, we use [x509](https://en.wikipedia.org/wiki/X.509) (the standard
format for public key certificates used for https and many other applications
of transport layer security). For example:

 * Members connecting via https to any of our sites, including their own web sites.
 * Members connecting securely to our mail server to send or relay email
 * Our internal relaying of email, e.g. between our [MX servers and the mail
servers that store email on behalf of our members](/postfix),
 * Communication between our [control panel](/control-panel) hub and the petal
   servers that execute the tasks requested.

For x509 certificats, we rely on [Let's Encrypt](https://letsencrypt.org/) to
verify and sign our certificate signing requests OR in some cases (e.g. in dev
or testing mode or for services that are only used internally) we run our own
certificate authority and sign our own certificates.

We also use our key server for other PKI tasks, such as generating DKIM keys
and certificates.

## Requests for keys and certificates

Requests for keys and certificates come from two sources:

 * Members requests https certificates for websites and DKIM certificates for
   domains via the members [control panel](/control-panel).

 * Admins indicate a need for X509 certificates via ansible using the
   `keyserver_certs` parameer

## The key server

To manage all of these keys and certificates, we run a single key server.

The key server is responsible for generating, renewing, distributing via ssh
and running any renewal triggers required for every certificate requested.

When ansible runs, it generates a single file, `/etc/ssl/ansible.certs.yml`,
listing all certificates it need.

When the control panel needs a certificate, it creates a file in
/etc/ssl/flower.certs.d for each request.

Every time a new request is added, `mf-certs-process` is run. `mf-certs-process`
looks for any certificate in the configuration files that either does not have
a key/cert pair or has an expired cert. 

Depending on whether the certificate is set to `letsencrypt` or `mfca`, the
script will either call `certbot` or it will generate a cert/key pair using our
ca. When creating a `letsencrypt` pair, it will configure the cert with a
renewal hook of `mf-certs-distribute <identifier>`.

For `mfca` cert requests, the script calls `mf-certs-distribute` every time a
new certificate is created or renewed. 

The `mf-certs-distribute` copies the keys and certs to the configured servers
and runs and renewal trigger commands - all via ssh. For example, certificates
for https web sites would be copied to all webstore and webproxy servers and
the renewal trigger would restart nginx or apache2.

## Validation

Keys and certificates issues via our own certificate authority require no
additional validation.

Lets encrypt certificates are validated via one of two
[challenges](https://letsencrypt.org/docs/challenge-types/):

 * All control panel request and most others are handled via HTTP-01. The
   keyserver runs a web server that accepts requests from any domain.  Our
   webproxy servers transparently redirect all requests to
   /.well-known/acme-challenge to the key server.

 * DNS-01: For domains that can't be validated via http (such as
   mail.mayfirst.org) we use DNS-01 (TBD - we will need to write some simple
   code - see [this comment](https://serverfault.com/a/989964) for how -
   replacing nsupdate with an api call to our control panel). 

## Authentication and permissions

Each server that needs a certificate must include the `keyserver_consumer:
true` setting in it's ansible file. That setting triggers the creation of a
local user called `keypusher` configured to allow access by the root user on
the key server. The user has sudo privileges to run any script starting with
`/usr/local/sbin/mf-certs-renew-*`. All servers have `mf-certs-renew-base`
which copies new keys and certs to `/etc/ssl/mayfirst/<name>/{privkey.pem,
fullchain.pem}`. Each requested certificate can include one or more additional
renew scripts to be executed as root. 

## Example

Here's an example of what you might include in an ansible server file:

    # Ensure your renew scripts are copied to the server. Renew scripts
    # live in the mayfirst role's files/cert-renew-scripts directory.
    keystore_renew_scripts:
      - mf-cert-renew-kibana
    # Trigger the creation of the `keypusher` user on this host.
    keystore_consumer: true
    keystore_certs:
      - name: report.mayfirst.org
        common_names:
          - report.mayfirst.org
          - www.report.mayfirst.org
        ca: letsencrypt
        challenge: http
        distribute_hosts:
          report001.mayfirst.org:
            - /usr/local/sbin/mf-cert-renew-base
            - /usr/local/sbin/mf-cert-renew-kibana
          webproxy001.mayfirst.org:
            - /usr/local/sbin/mf-cert-renew-base
            - /usr/local/sbin/mf-cert-renew-nginx

## Exceptions

In some cases it's preferable to run Letsencrypt locally on a server.

For example, a server that only runs postfix will have an A record pointing to
itself, not to the nginx proxy, so the `http` challenge won't work.

In these cases, you can configure the server with ansible veriables indicating
that it should run letsencrypt and handle certificate creation via the
`certbot` standalone server option.

For example:

    letsencrypt_certs:
      - name: monitor001.mayfirst.org 
        common_names:
          - monitor001.mayfirst.org
        renew_hooks:
            - /usr/local/sbin/mf-cert-renew-base
            - /usr/local/sbin/mf-cert-renew-postfix

