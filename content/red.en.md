+++
date = 2022-10-19T13:53:34Z
title = "Red: The May First control panel"
categories = ["red", "control-panel"]
+++

Red is the name of the May First control panel.

## Debugging failed items

When a control panel item is saved or deleted, one or more tasks are queued to
be executed (e.g. "run red-node-update on the target server" or "rebuild the
email databases" or "generate an https certificate"). The execution of the
queued items happens via a timer that runs every 15 seconds, not via the web.

Sometimes a control panel item has a "soft-error" or "hard-error" or seems to
be stuck in a pending state.

To learn more about what happened, you can use the `red-queue` command.

`red-queue [show|run] [--item-id ITEMID] [--restore] [--delete] [--disable]`

*Note: /usr/local/sbin/red-queue is a bash wrapper around /usr/local/share/red/ui/sbin/red-queue
that properly drops privileges before running.*

Choose either the `show` or `run` sub-command depending on whether you want to
examine the state of an item (`show`) or change its state (`run`).

Pass an ITEMID via `--item-id` to specify the item you want to examine or act
upon.

Optionally, when using the `run` sub-command, you can force the state of the
item with the `--restore`, `--delete` or `--disable` options.

## Granting SSH access to the shell servers

A user cannot access their web site server unless they first login to the
control panel, which grants them 24 hours of access. See the [shell](/shell)
page for details on how it works.

You can trigger that process by logging into red001:

1. Become the red user: `su - red -s /bin/bash`
2. `cd /usr/local/share/red/ui/sbin/`
3. `./red-grant-shell-access <user>`

If you want to access their site as their user, you also have to add your
public ssh key to their server access record.

## Generating the list of https certificates

When a user indicates that a web site in the control panel should have a
certificate, the control panel re-generates a file detailing all the
certificates required by web sites in the control panel. The file is copied to
`key001` and placed in (`/etc/ssl/red.certs.yaml`). Then it triggers the
`mf-copy-and-certify` script, which triggers Lets Encrypt to generate or update
a certificate for any site with a changed configuration. The script then
executes `mf-cert-distribute` to copy that certificate to all our web proxies
and weborigins that need it.

More info is on our [keyserver](/keyserver) page.

Sometimes there is an error and it's hard to "see" what is happening.
Generating a new file and examining it sometimes helps.  You can regenerate the
`red.certs.yaml` file by:

1. Become the red user: `su - red -s /bin/bash`
2. `cd /usr/local/share/red/ui/sbin/`
3. `red-update-certs-file`

That will only generate the file. If you want to also copy it to `key001` so you can
see all the scripts running in the foreground, pass the `--copy` argument.

## Generating web proxy configurations

When web sites are created or modified, the control panel copies all web proxy configurations
to all the web proxy servers configured in `/usr/local/share/red/etc/config.inc.php`.

See the [nginx documentation](/nginx/#adding-a-new-webproxy) for details on how
to initialize a new nginx proxy or otherwise manually copy configurations. ##

## Regenerating mail databases

We maintain flat file mail databases on our mx servers, cf servers and
filter servers to control what email addresses we allow in and how we
route them.

Every time a relevant record in the database is updated, the databases
are re-generated. See the [postfix](/postfix) and [dovecot](/dovecot) 
pages for more details.

To regenerate the cf server databases:

1. Become the red user: `su - red -s /bin/bash`
2. `cd /usr/local/share/red/ui/sbin/`
3. `red-initialize-dovecot-proxy-database`

To regenerate the postfix databases:

1. Become the red user: `su - red -s /bin/bash`
2. `cd /usr/local/share/red/ui/sbin/`
3. `red-initialize-postfix-databases`

To regenerate both:

1. Become the red user: `su - red -s /bin/bash`
2. `cd /usr/local/share/red/ui/sbin/`
3. `red-initialize-all-email-databases`


