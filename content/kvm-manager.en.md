+++
date = 2020-02-07T13:53:34Z
title = "KVM Manager: creating and managing virtual guests"
categories = ["install"]
+++

## Overview 

May First uses [KVM Manager](https://0xacab.org/dkg/kvm-manager) to manage
virtual guests on each of our physical host machines.

KVM Manager uses the `qemu-kvm` virtualization software.

KVM Manager is a set of simple bash scripts that help us by setting up new
virtual server instances and allows us to manage each instance via systemd. The
main benefits of KVM Manager are:

* Use isolation: nothing runs as root on the host. All virtual servers run via
  systemd as a non-provileged user.

* Easy console access using familiar tools. All kvm guests provide access to
  the console via screen. We can grant ssh access to the non-privileged user
  account on the host and allow any connecting user to connect to the kvm
  guests via `screen -x`.

KVM Manager specifically provides the following:

* Systemd integration: Two template systemd files are provided:

  * `kvm@.service`: Every kvm instance can be enabled or started via the
    service `kvm@<guest>.service`. This services handles setting up and tearing
    down the necessary network routing for the guest.
  * `kvm-screen@.service`: Every kvm instance has a corresponding
    `kvm-screen@<guest>.service` that controls the screen session providing
    access to the console.

* `kvm-creator`: creates a new virtual guest instances, specifically:

  * Creates a non-provileged user with a home directory.
  * Creates a logical volume to hold the operating system
  * Add udev rules to ensure the proper permissions to access the logical
    volume are created for the user.

## How to use it

KVM Manager is fully integrated into ansible. You can indicate which host a
server should be created on and all the details of the setup (including the
provision of extra data volumes) via the `kvm` variable.

* ansible will automatically resize partitions on the host after they have been
  created, but only if the new size is bigger then the old size. This step is
  handled by a script called `mf-manage-logical-volumes` which reads a
  configuration file `/etc/kvm-manager/logical-volumes.yml` that is written out
  by ansible.

* If a logical volume is deleted and a new logical volume is created, it will
  have a signature on it and the `lvmcreate` command will prompt for a
  confirmation to override it. Bah. That will cause ansible to simply hang. The
  only solution is to manually create that logical volume and re-run ansible
  since instructing `lvm` to auto wipe signatures seems like a bad idea. 

Here's an example of how to setup a kvm guest via ansible with comments:

    kvm: 
      # The host line indicates on which host this guest should be created.
      host: host-001.mayfirst.dev
      # Usually the guest name is the host name of the guest.
      name: flowerhub001
      # How much RAM should be allocated? You can change this later to increase
      # or decreate the RAM. You will need to restart the guest before it takes
      # effect but ansible will handle writing it out to the `env` file.
      ram: 5g
      # These details apply to the disk handed to the guest as the root disk which
      # will appears as /dev/vda or /dev/sda depending on the driver. You can change
      # these after the guest has been created and the disk will be resized, but the
      # change will only take effect after the guest is rebooted.
      root_disk:
        size: 20g
        volume_group: vg0 
        # You have to specify a driver. `virtio-blk-pci` should always be the
        # default and is the best option *unless it's an SSD disk* in which
        # case you should specify `scsi-hd` so the guest can properly using
        # [trim](https://en.wikipedia.org/wiki/Trim_(computing)).
        driver: virtio-blk-pci
        # kvm-manager has to index each disk separately so it can specify HDA,
        # HDB, HDC, etc, however they won't # necessarily show up with this
        # letter on the guest. This letter has to be unique within a given guest,
        # not unique for the host.
        index: A
      # Like the root disk, you can change the size of a disk after it has been created
      # and it will automatically be re-sized, but a reboot is required.
      data_disks:
        - name: data0001
          size: 250g
          volume_group: vg1
          # See driver comment above.
          driver: scsi-hd 
          index: B
        - name: data0002
          size: 250g
          volume_group: vg1
          driver: scsi-hd 
          index: C

NOTE: You have to limit ansible to just the metal server when you first run it,
otherwise it will complain that the container guests are not reachable (because
they have not yet been created).

## What to do after ansible runs

A lot of this process is automated, but there are still quite a few steps to run by hand.

### Run the installation

After your first run, the guest will be created and booted into the installer.

You have to ssh into the console (`$guest@$host.mayfirst.org`) and type: `screen -x`.

Then, hit enter to start the installer.

You also have to click through the warning about no swap.

### Logging in and initializing 

Once the guest has booted, you can login with the username `root` and no password.

Change the password and add it to [keyringer](/keyringer). 

Then, run `/root/mf-initialize-server`, which will ensure the network is setup,
install  minimal packages to get ansible to work and will publish the sshfp dns
records so when you ssh into the server directly, you will know you have the
right fingerprint.

### Setting partitions

You can run `cat /proc/partitions` to see a list of the partitions you have. Ensure they are all there!

Next, format each one (`mkfs -t ext4 /dev/DEVICE`).

Check the block id of the device (`blkid /dev/DEVICE`).

Edit `/etc/fstab` to add device so it is automounted in the right place.

Ensure it works by manually mounting with `mount /path/in/filesystem`

### Run ansible

Now you should be ready to push ansible to the guest.

## Maintenance

Sometimes you want to make a change to an existing KVM guest.

The most common change is to add or modify a logical volume.

To avoid running the full ansible playbook, you can pass `--tags lvm` to just
run the tasks related to provisioning logical volumes.

## Trouble shooting

### ansible hangs

If, when running ansible on the bare metal server, it just hangs on:

    TASK [run kvm creator for each guest on this host]

Then it may mean that `lvm` is creating a logical volume from a volume group in
which there is already a logical volume signature. Out of an abundance of
caution, we don't use the flag to ignore this error.

To get around this problem, you'll need to manually run the `kvm-creator`
command on the host.

It should be in this format:

    kvm-creator create SERVERNAME VOLUMEGROUP DISKSIZE RAM

For example:

    kvm-creator create samba001 vg_llosa0 25g 2g

Then, re-run ansible.

### Installer says no kernel modules found

If you get an error in the installer saying "No kernal modules were found"
then, delete `/usr/local/share/ISOs/default.iso` and the .iso file named after
your guest and re-run ansible. This error means that our base installer is out
of date and needs to be refreshed.

### Data block device is not available in the guest

If your data block device does not show up in your guest as expected, ensure
that the permissions are set correctly on the host:

First, check where your device mapper is symlinked, e.g.:

    0 severo:~# ls -l /dev/mapper/vg0-data0002
    lrwxrwxrwx 1 root root 7 Mar 26 14:18 /dev/mapper/vg0-data0002 -> ../dm-9
    0 severo:~#

Then, check the permissions on the actual device:

    0 severo:~# ls -l /dev/dm-9 
    brw-rw---- 1 root disk 253, 9 Mar 26 14:18 /dev/dm-9
    0 severo:~#

The group ownership should be set to the name of the guest (e.g. container001).

Ensure that the udev rule is correct:

    0 severo:~# cat /etc/udev/rules.d/92-kvm-creator-data0002.rules 
    ACTION=="change", SUBSYSTEM=="block", ATTR{dm/name}=="vg0-data0002", GROUP="container001"
    0 severo:~#

(If it is not correct, you may need to re-run ansible and/or check your configuration.)

Then, re-trigger udev to get it run: `udevadm control --reload-rules && udevadm trigger`
