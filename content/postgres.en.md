+++
date = 2024-07-13T13:53:34Z
title = "Postgres"
categories = ["sql"]
+++

Postgres database have to be uprade as manually after a Debian upgrade.

1. First become the postgres user: `su - postgres`
2. Next, list all clusters: `pg_lsclusters`
3. See if any clusters have databases other than "postgres", "template0", and "template1": `psql --port NNNN --list` (replace NNNN with the port number returned by the `pg_lsclusters` command, start clusters if necessary with: `pg_ctlcluster VERSION main start`)
4. When the most recent version has no other databases other than the defaults, you can drop it and then upgrade from the version that has a database. As root: `systemctl stop postgresql@VERSION-main`. As the postgres user: `pg_dropcluster VERSION main`
5. Upgrade the cluster with the databases you want to keep. Postgres will always upgrade to the most recent version: `pg_upgradecluster VERSION main`
