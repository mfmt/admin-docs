+++
date = 2023-12-28T13:53:34Z
title = "Email Abuse and Deliverability"
categories = ["mail", "intrusion-detection" ]
+++

We can access data about email complaints from several sources.

## Our official abuse email address.

Our official email for receiving abuse complaints is `abuse [@] m.o`.

This address is configured with our upstream provider and via the IP addresses
assigned to us, so any one reporting complaints can use those sources to know
which email address to use. Also, our upstream provider uses it to forward
complaints they receive referring to us.

This email address is checked by our [Abuse Report
Parser](https://code.mayfirst.org/mfmt/abuse-report-parser). 

It escalates abuse email that are not automated messages in the [abuse
reporting format](https://en.wikipedia.org/wiki/Abuse_Reporting_Format) to our
info email and sends a summary of abuse reports on a weekly basis to our
support address.

You can check the data by creating a shell account for yourself in the
`abuse.mayfirst.org` hosting order. Then, when logged in via ssh, run
`abuse_report_parser.py`.

See the [gitlab page](https://code.mayfirst.org/mfmt/abuse-report-parser) for
the full set of sub-commands.

## The big email providers

### Gmail

Google doesn't send complaints via email - instead all complaints are
aggregated and made available via their Postmaster tools. We have one Google
account with access to the postmaster tools available here:
https://postmaster.google.com/managedomains

We can track complaints against email sent with our main domain.

Google also has a [Feedback loop
system](https://support.google.com/mail/answer/6254652). This system could
allow us to insert an Email header in each outgoing email message that would
allow us to track complaints in a more fine-grained manner, but it requires
each sending domain to be added and verified to our Gmail Postmaster Tools
account, which would require us to tediously register every domain used by
every member.

At this time, only mayfirst.org is registered.

### Yahoo

Yahoo also has a feedback loop. Unlike Gmail, it does send complaints to our
generic abuse email address (hooray!). But, like Gmail, it only works for
domains that are verified. Using our standard Yahoo account, I have created
an account for mayfirst.org. More details: https://in-stage.help.yahoo.com/kb/SLN26007.html

And, here is the link to the dashboard: https://senders.yahooinc.com/

In addition, the [Yahoo Sender FAQ](https://senders.yahooinc.com/faqs/) has
more details information, including a link to a [sender support request
form](https://senders.yahooinc.com/contact/#sender-support-request) that can be
used to ask questions about blocked email.

### Microsoft

Microsoft has both a Smart Network Data Services program and a Junk Email
Reporting Program (see their [page of
services](https://sendersupport.olc.protection.outlook.com/pm/services.aspx)).
We have a specific snds Outlook account that can be used to access the
[dashboard page](https://postmaster.live.com/snds).

It seems that all of our IP ranges (except our /24 on loan) are already
registered by our upstream, which is why we are getting those forwarded from
our upstream to our abuse email address. I just added our /24 as well.

Since we only have a small portion of the upstream IP addresses, it seems that
our upstream is in control of this resource and has shared info with us and
other clients of theres. It's quite confusing. Somehow the Junk Mail reporting
is part of this dashboard too. We already getting ARF emails to our abuse
address from Hotmail that are relayed through our upstream provider.

## Validity

I discovered a service called Validity that has a [Feedback Loop
service](https://fbl.validity.com) that claims to send us ARF feedback messages
from dozens of providers. I created an account, but when I tried to submit our
list of CIDR ranges, they were not able to determine how to verify them so I've
given up.
