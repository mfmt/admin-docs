+++
date = 2019-08-21T13:53:34Z
title = "Hacking Ansible"
categories = ["ansible"]
weight = -50
+++

If you want to hack on our code, start by checking out the repository:

    git clone git://gitlab.com:mfmt/seed.git

## Quick Start

## Preparing your host

Our playbook has been developed on a Debian system (buster and bullseye).

Minimally, you will need:

    apt install docker.io ansible debootstrap

If you are running ubuntu, you may want to install [Docker Community
Engine](https://docs.docker.com/install/linux/docker-ce/ubuntu/) instead. The
version in Debian Buster and Bullseye seems to work properly.

If you are running Debian bullseye or earlier, you will need a more recent
version of python3-yaml, which you can install with:

    apt install python3-pip
    apt purge python3-yaml
    pip3 install PyYAML

You also have to have a recent version of ansible. The Debian bullseye version
works. If you have an older machine, then you'll need:

    pip3 install ansible

## Copying the default inventory file 

The git repo ships with a default dev inventory file.

To get started on your own development machine, copy the
`inventories/dev/hosts.yml.default` to `inventories/dev/hosts.yml`.

You can start with the defaults - which set up a minimal number of hosts
focused on testing the full round trip of control panel, email and web sites.
As you continue working with the code, you may choose to enable other hosts.

## Create your docker containers

Then, you will need to [create a series of docker
containers](/ansible-docker-initialize) representing all the server groups we
use.

Warning: running all of the docker containers takes up a lot of RAM. If you
have less than 16GB on your workstation, you may need to close any programs
that consume significant amounts of RAM before beginning.

## Make it happen 

If you are ready, you run the command to apply all of our roles with:

    ./seed-playbook-dev

See our [ansible-vault](/ansible-vault) for more information about the vault business.

## Slow start

### Layout

The git repo is layed out like a typical ansible playbook. The main directories
and files of interest are:

 * inventories - see dev/hosts.yml - that is the list of hosts we will test
 * `site-dev.yml` and `site-live.yml` - these files determine which roles are
   applied
 * roles - this directory contains all the playbooks. Most coding happens with
   roles


### Philosophy

There a million decisions that have to be made for every small piece of
software added to this repository. Here are some guiding principles to help
make them:

 * Keep it simple: this repo is for May First, it's not intended to be useful
   for anyone else (except as a starting point). Only implement options we need
   right now.
 * Prioritize easy upgrades: When setting configuration files, used conf.d
   directories when available. When modifying configuration files included in a
   package, use `lineinfile` and `blockinfile` rather than copying our version
   of the file. We want to always incorporate upstream configuration defaults
   whenever a package is upgraded.
 * Prioritize a few key roundtrip tests rather then loads of smaller unit
   tests. We don't want to spend our lives maintaing test code and we want to
   focus on tests that demonstrate multiple systems are working together over
   tests that demonstrate a relatively simply operation works. For example: a
   test the demonstrates an email message can be sent and received is better
   then a test that demonstrates a Maildir is created.

### Tips

Ansible is slow! If you want to only run a few tasks, use the
[tags](https://docs.ansible.com/ansible/latest/user_guide/playbooks_tags.html)
option:

 * To limit to a given role, run: `./seed-playbook-live --tags name_of_role foo.mayfirst.org`
 * Or temporarily, add: `tags: debug` (or any value) to a task and run with `--tags debug`.

### Elastic Search and friends

By default, elasticsearch, kibana, journalbeat and metricbeat are disabled
because they are memory hogs.

If you want to test them out, look for their respective `_enabled` settings in
`inventories/dev/hosts.yml` and change them to `true`.

However, `elasticsearch` will not run properly unless the `max_map_count`
setting on your host is at least 262144. 

    sudo sysctl -w vm.max_map_count=262144

That temporarily sets it (but won't retain on reboot). If you want it to
survive a reboot, add a file in /etc/sysctl.d/local.conf with:

    vm.max_map_count=262144

### Adding roles

To initialize an empty role directory:

    cd roles/admin
    ansible-galaxy init rolename

### Git Tags

Git tags (not ansible tags!) are only added if the commit can be built and tested without errors.

If you are having trouble building out all the images in the dev inventory,
trying going back to the last git tag and seeing if that works.
