+++
date = 2019-12-23T13:53:34Z
title = "LDAP"
categories = ["ldap"]
+++

Our primary user (and all resources) database is the MySQL database that backs
the [control panel](flower).

However, the control panel synchronizes all users with an LDAP database which
is used behind the scenes by several services:

 * postfix (smtp) uses the LDAP database to lookup domain names and email
   addresses to determine whether we should accept or reject a message for a
   given address and the username and server for a given email address to
   determine how to relay the message to the final destination.

 * dovecot (imap/pop) uses the LDAP database to verify login credentials and
   determine where to proxy the connection for a given user.

 * other services such as Nextcloud, Prosody, and Discourse use LDAP to
   authenticate users.

Neither the public nor members have direct access to the LDAP database.

If you are looking for a general reference to help you understand the concepts
behind LDAP, the [Zytrax book](https://www.zytrax.com/books/ldap/) is by far
the best read.

We run `slapd` from the OpenLDAP project as our LDAP server and keep as close
as possible to the default Debian configuration.

The slapd ansible role's main.yml tasks file is well-commented and provides a
good, detailed explanation for how our LDAP server is configured. Here's a
brief summary:

 * We start with a vanilla Debian slapd installation. We use the newer
   `cn=config` style online configuration system rather than the slapd.conf file,
   which means rather than editing a file and reloading `slapd` to change the
   configuration, we have to run ldap commands that make changes to the config
   ldap database itself (which changes the configuration on the fly).

 * We create an admin user and password and store it in /root/.slapdpassword on
   the slapd server. We also create a /root/.ldaprc file with our BINDDN
   (username) settings. With these files in place, we can run commands that
   add/delete/modify entries in the LDAP database with the format:
   `ldap<command> -xy /root/.slapdpassword <args>`. `-x` means use simple
   authentication and `-y` specifies the path to the password. 
   
   In slapd land, different access is required to make changes to the slapd
   configurations. By default in Debian, the only way to make configuration
   changes is by accessing `slapd` via a unix socket as the root user. You can
   do that with: `ldap<command>` -Y EXTERNAL -H ldapi:/// <args>`. -Y EXTERNAL
   means the user will be authenticated externally to `slapd` (i.e. via
   username when accessing via the socket) and -H ldapi:/// is how you specify
   that you want to access `slapd` via the default path to the unix socket.

 * Next, we add some organizational units (ou's). Organizational units divide
   our ldap entries by the type of entries. We setup three kinds of entries: 

   * users: all the regular member users in our system. Every "login" in the
     control panel corresponds to a user in LDAP. If an email address is
     configured to deliver email to a login, then the user ldap entry gets an
     email address attached to it. More on users below.

   * emailDomains: any domain name that should be allowed to receive email by
     our servers must be added in this organizational unit. The addition of
     email domains is triggered by the addition of an MX domain name record.

   * emailAliases: All email addresses that are aliases to other email
     addresses are listed in this organizational unit. This records include
     email addresses that forward off site and email addresses that forward to
     other internal email addresses. However, if an email address should
     deliver directly to a login, then it is not listed here and instead is
     added to a user record. 

 * Then, we add system users. These are the users (e.g. postfix, dovecot) that
   need special permission to lookup our regular users to figure out if they
   can login, how to deliver their email, etc. 

 * Lastly, we depart from the Debian plain configuration in a few small, but
   important ways:
    
   * We add a new schema that allows slapd to handle fields related to the
     delivery of email. This schema allows us to record things like what server
     a user's mailbox is on, etc.

   * We add extra indices for fields that we expect to be searched on frequently.

   * We disable anonymous binding (that means you must have a username and
     password if you want to connect to the server).

   * We disable the ability for all authenticated users to read the whole database.

   * We allow our system users the permission to read the whole database.

 * objectClasses: Every record in an ldap database must belong to at least one
   objectClass, which is the schema that defines which fields that record can
   have (and which fields it must have). We use inetOrgPerson for all users by
   default. If they get configured to receive email, then the postfixAccount
   objectClass is added.
