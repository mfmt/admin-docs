+++
date = 2021-10-29T13:53:34Z
title = "Restore from Backup"
categories = ["backup"]
+++

To restore from the backup, always start from the server hosting the data being
restored. Do not restore from the backup server.

From the source host (`source_host`), examined `/etc/backup.d/71_backup.borg`.
Look for the `host`, `port`, `user`, and `directorty` values.

With this info, execute the command:

    borgbackup list ssh://${source_host}-sync@${host}:${port}${directory}

You should see a list of dated backup archives (`archive`).

Next, get a file list of the available files with:

    borgbackup list ssh://${source_host}-sync@${host}:${port}${directory}::archive

Next, `cd` into the directory you want to restore the files. Determine the `path` of the file or files you want to restore (as it existed on the source host) and type:

    borgbackup extract --dry-run ssh://${source_host}-sync@${host}:${port}${directory}::$archive $path

If it looks good, try again without `--dry-run`.
