+++
date = 2020-02-06T13:53:34Z
title = "Network Boot"
categories = ["install"]
weight = -100
+++

## Network Boot

Sometimes we have to boot strap a server into an operating system via the
network.

There are two main ways we do that:

### PXE Boot

PXE Boot. Almost all servers have the option to boot from a network. The
cabinet with the server you want to boot must have a server configured to
provide TFTP and DHCP and have the proper images. In telehouse, `clr` is 
currently setup to provide netboot services.

#### Setup

To setup a server to be the DHCP/TFTP server:

 * Install: `apt install memtest86+ tftpd-hpa isc-dhcp-server nginx`
 * Make the directory:
```
    mkdir -p /srv/tftp/debian-live
    mkdir -p /srv/tftp/boot/grub
    mkdir -p /srv/tftp/memtest/
```
 * Edit `/etc/nginx/sites-enabled/default` and set the root directory to `/srv/tftp`. Restart nginx.
 * Visit https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/ and download `debian-live-<version>-amd64-standard.iso` to the TFTP server.
 * Mount it: `mount -o loop -t iso9660 /{full path where you have the iso stored}/debian-live-<version>-amd64-standard.iso /mnt/` 
 * Create `/srv/tftp/debian-live/<version>`
 * Copy the contents: `cp -R /mnt/* /srv/tftp/debian-live/<version>/`
 * Unmount: `umount /mnt`
 * Copy memtest: `cp /boot/memtest86+.bin /srv/tftp/memtest/`
 *  Edit /srv/tftp/boot/grub/grub.cfg. You will want something along the lines of:
 ```
set timeout=300
set default="0"

serial --unit=0 --speed=115200 --word=8 --parity=no --stop=1
terminal_input serial
terminal_output serial

### BEGIN /etc/grub.d/05_debian_theme ###
set menu_color_normal=cyan/blue
set menu_color_highlight=white/blue
### END /etc/grub.d/05_debian_theme ###

menuentry 'debian live (console)' --class debian --class gnu-linux --class gnu --class os {
       insmod part_msdos
       insmod ext2
       set gfxpayload=keep
       set root='(pxe)'
       echo    'Loading linux ...'
       linux /debian-live/<version>/live/vmlinuz verbose console=ttyS0,115200n8 boot=live components fetch=http://162.247.75.122/debian-live/<version>/live/filesystem.squashfs
       echo    'Loading initial ramdisk ...'
       initrd  /debian-live/<version>/live/initrd.img
}

menuentry "Memory test (memtest86+) Serial (S0)" {
        insmod part_msdos
        insmod ext2
        set root='(pxe)'
        linux16 /memtest/memtest86+.bin  console=ttyS0,115200n8
}
menuentry "Memory test (memtest86+) IMPI Sol (S1)" {
        insmod part_msdos
        insmod ext2
        set root='(pxe)'
        linux16 /memtest/memtest86+.bin  console=ttyS1,115200n8
}
```
 * Install the network-bootable grub: `grub-mknetdir --net-directory=/srv/tftp`
   * Note: the `grub-mknetdir` command will make grub available for booting over the network, but... it will install grub in a way that is appropriate for booting the computer you run it on. In other words, if your run this command on a computer that boots via EFI, then you will install a version of grub that will bot via EFI.
   * You may need to review `man grub-mknetdir` and `man grub-mkimage` and/or copy files from `/srv/tftp` on clr or another existing server with this stack installed.
 * Determine the MAC address of the network device you are using
 * Edit /etc/dhcp/dhcpd.conf with something along the lines of the following. In this example  `192.168.1.1` is the IP address of the DNS server, `192.168.1.2` is the IP of the server hosting tftp/dhcpd, `192.168.1.10` and `192.168.1.15` is the range of IPs that will be handed out and `foo` and `00:16:b8:35:d1:ec` is the name and MAC address of the server that will be allowed to get an IP address.

 ```
option domain-name "mayfirst.org";
option domain-name-servers 192.168.1.1;
default-lease-time 600;
max-lease-time 600;
authoritative;

shared-network mfpl-telehouse {
 subnet 192.168.1.0 netmask 255.255.255.0 {
   range 192.168.1.10 192.168.1.15;
   option routers 192.168.1.1;
   next-server 192.168.1.2;
   filename "/boot/grub/i386-pc/core.0";
   deny unknown-clients;
 }
}

host foo {
   hardware ethernet 00:16:b8:35:d1:ec;
}

```

 * start the DHCP server: `systemctl start isc-dhcp-server`
 * start the tftp server: `systemctl start tftpd-hpa`
 * Configure the target server to boot from the network device

The Debian Live CD default username is: `user` and password is: `live`

You can then run `sudo -i` to gain root access.

After you are done, be sure to shutdown the dhcpd and tftp services 

### IPMI Samba boot

Alternatively, if setting up DHCP/TFTP is not feasable and the server is
running IPMI, you can configure IPMI to boot from an ISO shared via Samba.

In IPMI, choose the "Virtual Media" menu item.

Then, choose CD-ROM

Then:

 * Share Host: 192.168.0.1
 * Path to Image: `\netboot\debirf-rescue_stretch_4.9.0-9-amd64.iso`

Note: When you boot to this ISO image, you will get a grub screen. grub will be configured to provide a console via tty0, which won't work under IPMI. So, before you boot, you need to edit the Grub boot parameters - changing:

```console=tty0 console=ttyS0,115200n8```

to

```console=tty0 console=ttyS1,115200n8```

#### Setup
 * Install samba: `apt install samba smbclient wget`
 * Edit /etc/samba/smb.conf, add the following (assuming local ip of `192.168.0.1` on `eth1`):
 ```
    interfaces = 192.168.0.1/24 eth1
    bind interfaces only = yes
    [netboot]
        comment = Network bootable ISOs
        path = /usr/local/share/ISOs
        guest ok = yes
        read only = yes
        browseable = yes
```
 * Create directory for ISOs: `mkdir /usr/local/share/ISOs`
 * Download debirf: `cd /usr/local/share/ISOs && wget http://debirf.cmrg.net/autobuilds/amd64/stretch/rescue/debirf-rescue_stretch_4.9.0-9-amd64.iso`
 * Restart smb: `systemctl restart smbd`
 * Test: `smbclient \\\\foo\\netboot -I 192.168.0.1 -N` followed by `ls` and you should see the debirf image listed.
