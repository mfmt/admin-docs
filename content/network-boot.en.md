+++
date = 2020-02-06T13:53:34Z
title = "Network Boot"
categories = ["install"]
+++

## Network Boot

Sometimes we have to boot a server into an operating system booted from the
network.

There are two main ways we do that:

### PXE Boot

PXE Boot. Almost all servers have the option to boot from a network. The
cabinet with the server you want to boot must have a server configured to
provide TFTP and DHCP and have the proper images. In telehouse, `clr` is 
currently setup to provide netboot services.

#### Setup

To setup a server to be the DHCP/TFTP server:

 * Install: `apt install memtest86+ tftpd-hpa isc-dhcp-server` (replace "9" with the version you want)
 * Make the directory:
```
    mkdir -p /srv/tftp/debirf/<release-name>/amd64
    mkdir -p /srv/tftp/boot/grub
    mkdir -p /srv/tftp/memtest/
```
 * Visit http://debirf.cmrg.net/autobuilds/amd64/ and download both the vmlinuz and file ending in .cgz to the /srv/tftp/debian/<release-name>/amd64 directory.
 * Copy memtest: `cp /boot/memtest86+.bin /srv/tftp/memtest/`
 *  Edit /srv/tftp/boot/grub/grub.cfg. You will want something along the lines of:
 ```
set timeout=300
set default="0"

serial --unit=0 --speed=115200 --word=8 --parity=no --stop=1
terminal_input serial
terminal_output serial

### BEGIN /etc/grub.d/05_debian_theme ###
set menu_color_normal=cyan/blue
set menu_color_highlight=white/blue
### END /etc/grub.d/05_debian_theme ###

menuentry 'stretch debirf (console - S0)' --class debian --class gnu-linux --class gnu --class os {
        insmod part_msdos
        insmod ext2
        set root='(pxe)'
        echo    'Loading linux ...'
        linux   /debirf/stretch/amd64/vmlinuz-4.9.0-5-amd64 verbose console=ttyS0,115200n8
        echo    'Loading initial ramdisk ...'
        initrd  /debirf/stretch/amd64/debirf-rescue_stretch_4.9.0-5-amd64.cgz
}
menuentry "Memory test (memtest86+) Serial (S0)" {
        insmod part_msdos
        insmod ext2
        set root='(pxe)'
        linux16 /memtest/memtest86+.bin  console=ttyS0,115200n8
}
menuentry "Memory test (memtest86+) IMPI Sol (S1)" {
        insmod part_msdos
        insmod ext2
        set root='(pxe)'
        linux16 /memtest/memtest86+.bin  console=ttyS1,115200n8
}
```
 * Install the network-bootable grub: `grub-mknetdir --net-directory=/srv/tftp`
 * Determine the MAC address of the network device you are using
 * Edit /etc/dhcp/dhcpd.conf with something along the lines of the following. In this example `209.51.180.240` is the IP of the server hosting tftp/dhcpd, `209.51.180.238` and `209.51.180.239` are the two IPs is will hand out and `clr` and `00:26:b9:35:c1:eb` is the name and MAC address of the server that will be allowed to get an IP address.

 ```
option domain-name "mayfirst.org";
option domain-name-servers 216.66.22.34;
default-lease-time 600;
max-lease-time 600;
authoritative;

shared-network mfpl-telehouse {
 subnet 209.51.180.224 netmask 255.255.255.224 {
   range 209.51.180.238 209.51.180.239;
   option routers 209.51.180.225;
   next-server 209.51.180.240;
   filename "/boot/grub/i386-pc/core.0";
   deny unknown-clients;
 }
}

host clr {
   hardware ethernet 00:26:b9:35:c1:eb;
}

```

 * start the DHCP server: `systemctl start isc-dhcp-server`
 * start the tftp server: `systemctl start tftpd-hpa`
 * Configure the target server to boot from the network device

After you are done, be sure to shutdown the dhcpd and tftp services 

### IPMI Samba boot

Alternatively, if setting up DHCP/TFTP is not feasable and the server is
running IPMI, you can configure IPMI to boot from an ISO shared via Samba.

In IPMI, choose the "Virtual Media" menu item.

Then, choose CD-ROM

Then:

 * Share Host: 192.168.0.1
 * Path to Image: `\netboot\debirf-rescue_stretch_4.9.0-9-amd64.iso`

Note: You need to edit the Grub boot parameters - changing:

```console=tty0 console=ttyS0,115200n8```

to

```console=tty0 console=ttyS1,115200n8```

#### Setup
 * Install samba: `apt install samba smbclient wget`
 * Edit /etc/samba/smb.conf, add the following (assuming local ip of `192.168.0.1` on `eth1`):
 ```
    interfaces = 192.168.0.1/24 eth1
    bind interfaces only = yes
    [netboot]
        comment = Network bootable ISOs
        path = /usr/local/share/ISOs
        guest ok = yes
        read only = yes
        browseable = yes
```
 * Create directory for ISOs: `mkdir /usr/local/share/ISOs`
 * Download debirf: `cd /usr/local/share/ISOs && wget http://debirf.cmrg.net/autobuilds/amd64/stretch/rescue/debirf-rescue_stretch_4.9.0-9-amd64.iso`
 * Restart smb: `systemctl restart smbd`
 * Test: `smbclient \\\\foo\\netboot -I 192.168.0.1 -N` followed by `ls` and you should see the debirf image listed.
