+++
date = 2024-02-14T13:53:34Z
title = "Red Validation"
categories = ["red", "control-panel"]
+++


Since we have tightened our validation rules without ensuring existing data
meets the new validation rules, we have a lot of data in the control panel that
will trigger a validation error when you try to edit, disable or delete it.

The following are common [red](/en/red) validation errrors and how to fix them:

## A DNS record with the same value already exists for example.org / txt.

This error may have a different domain than "example.org" or a different DNS type than "txt".

This validation rule stops you from creating two DNS records that have the same
value. 

If you get this error when enabling a hosting order or record, you have to
search the control panel for the duplicate DNS record and then delete one of
them. The duplicate might be a in a different hosting order.

## The domain (example.org) is not entered as a DNS record under your membership.

We recently made it a requirement that a domain name listed in a web configuration
must also be listed in the DNS section. That is true even if the domain name is
not managed by our DNS servers.

If you get this error, you will need to add the domain name to the DNS section, either
as as "ALIAS" record (if the hosting order is using the new infrastructure) or an A
record if it is using the old infrastructure.

## Generating your https certificate failed. 

The full error is:

> Generating your https certificate failed. Please ensure that all the domains listed for this configuration are properly assigned the IP address for the server. You can always add more domains at a later date. Or you can change your site to http only for now.

This error can happen if a web site's DNS entries are not currently pointing to our servers. There is nothing we can do except explain the situation to the member.

Or, it can happen when enabling a hosting order. The DNS entries are enabled first, but may not
be fully active when the web configuration is enabled. You can resolve this problem by simply re-editing the web configuration and submitting it.
