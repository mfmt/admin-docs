+++
date = 2021-05-03T13:53:34Z
title = "Jitsi Meet"
categories = ["jitsimeet"]
+++

# Jitsi Meet

[Jitsi Meet](https://github.com/jitsi/jitsi-meet) is a web-based video
conferencing tool based on XMPP and HTML5 web browser video standards. (It is
easily confused with with Jitsi, which is a XMPP client.)

The Jitsi Meet world has many components:

 * [The server](/jitsimeet-server): The server component includes the nginx web
   server, prosody XMPP server, jitsi-videobridge2 (the main unique component
   that makes everything work), and some html and javascript glue to tie it all
   together.
 * [Jibri](/jitsimeet-jibri): The jitsimeet recording and streaming service.
   Typically installed on a separate server, jibri can connect to a running
   jitsimeet conference and record or stream and conference.
 * [Jigasi](/jitsimeet-jigasi): The jitsimeet gateway to SIP allows for
   incoming and outgoing connections to the PSTN (via a SIP gateway like one
   provided by Twilio).
 * The client: People join a jitsimeet conference either via a regular web
   browser (Firefox and Chrome work best) or through a dedicated client ([which
   is still not easily
   found](https://github.com/jitsi/jitsi-meet-electron/issues/265)) available
   for desktop, android or iPhone.
 * In addition to our standard Jitsi Meet web site, we run a special
   [intrepretation enabled](/jitsimeet-simultaneous-interpretation) version via
   an iframe.
