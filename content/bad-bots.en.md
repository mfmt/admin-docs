+++
date = 2023-11-06T13:53:34Z
title = "Stopping Bad Bots"
categories = ["web"]
+++

## Overview

This page describes our strategy for throttling or blocking bots from accessing
web sites on our servers.

Bots are programs making web site requests that are not directly driven by a
human.

There is some gray area. For example, if a user posts a link on Bluesky, a
Bluesky bot will lookup the web page and retrieve the name of the page and
possibly an image from the page. That's technically a bot, but it's one
responding directly to a human's request.

Also, Stripe, PayPal and Authorize, among many companies, use bots to notify
CRM databases that a recurring payment has been processed. These are 100% bots,
but ones that should never be throttled or blocked.

## General architecture

Our web proxy servers use two files to manage bots:

1. `conf.d/bad-bots.conf`: This file defines several maps and geo stanzas to
   identify various bots and also defines `limit_req_zones` that can be used to
   throttle requests.
2. `snippets/bad-bots.conf`: This file is automatically included in each
   web site configuration and includes `if` statements and `limit_req`
   statements to set the blocks and rate limiting for each site.

## Outright blocking

### User Agents

We periodically download the list of known bad user agents from the [Nginx Ultimate Bad Bot Blocker
project](https://github.com/mitchellkrogza/nginx-ultimate-bad-bot-blocker).

Any request from a matching user agent is blocked with a return code of: `480`.

### Bad referers

We similarly download the Bad Bot Blocker project's list of bad referers and
block them with a return code of `481`.

### Bruce

Our [Bruce Banner](/bruce) program uses `nftables` to block IPs that repeatedly
make suspicious connections.

However, if a requests from one of these IPs comes in via Cloudflare, then we
won't block it. So, we have a systemd timer, `mf-nft-ips-to-nginx` that runs
every hour and translates the list of IPs banned into a mapped nginx variable.
We block them with code `482`.

### Spamhaus

[Spamhaus](https://spamhaus.com/) provides a free do not route list of IP
ranges that are used by known spammers. We block these with code `484`.

### May First Custom

Lastly, we maintain our own custom list of ranges that we outright block with
code `485`;

## Throttling

In addition to outright blocking, we also throttle. These will all get `429`
error codes. But, you can look in the site's error log to find out which
throttle was imposed on the request.

### Good bots

We maintain a custom list of "good" bots - bots that we tolerate, but still rate limit
to just 6 requests per minutes. These are in the zone `good_bot_user_agent_limit`.

### Social bots

A special class of "good" bots are used by social networking sites to post links. These
are *also* used for scraping, but if we throttle them too hard, then our users complain,
so they get to make 30 requests per minute. These are in the zone `social_Bot_user_agent_limit`.

### No user agents

If you request a page without a user agent, you only get six requests per minute. Since
some payment processors have no user agents, we also maintain a list of IP addresses
that are excepted from this rule. These are in the zone `no_user_agent_limit`.

## Maintenance

We have a script in our ansible nginx-proxy role's default folder that
autogenerates the lists of user agents, referers and IPs from the Nginx Bad Bot
project and spamahus. This script should be periodically run to update the list
of bots to ban.

And, on monitor002, there is a script
(`/root/elastic-scratch/elastic-web-unique-user-agents`) that will generate the
most frequently seen user agents. Any user agent seen more than 1,000 times
(that is clearly a bot) should be added to the good bots variable in the
`hosts.yml` file.
