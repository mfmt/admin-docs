+++
date = 2021-01-01T13:53:34Z
title = "Alerter: Send notifications to administrators via the right channels"
categories = ["alerts"]
+++

When it's time for a [monitoring](/monitor) tool to push an alert to an
administrator, it calls the `alert` command with the appropriate arguments:

 * `--team` - either ptp or mayfirst, depending on which team should receive
   the alert.

 * `--priority` - either critical or warning depending on the severity (see the
   [monitor](/monitor) page for an explanation of the difference between the
   two)

 * `--body` - the body of the message to send.

 * `--subject` - optionally provide a subject (only used for email alerts)

The `alert` command consults the `/etc/alerter.yml` file to determine how to
send the alert. It currently supports sending via `email` or `signal`.

The `alerter` system depends on a working smtp service and the
[signal-cli](/signal-cli) service. We also have a document explaining
[what to do when we get an alert](/alert-response).
