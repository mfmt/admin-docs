+++
date = 2024-02-03T13:53:34Z
title = "Domain Name Resolution"
categories = ["dns"]
+++

There are many kinds of domain name servers in our network:

 * function: `authoritative` vs `caching`
 * scope: `public` vs `internal`
 * profile: `origin` vs `copy` vs `static`

### Caching servers

Our `caching` DNS servers are also known as resolving DNS servers.

Every physical host runs `unbound` directly, providing DNS caching services for
all hosts in our network. This service is configured to only respond to
requests from IP addresses in our ansible playbook, including hosts in other
locatins.

Each server's `/etc/resolv.conf` file includes lines for at least two of the IP
addresses of a phsyical host server providing this service.

Additionally, each of our caching servers references our "stub domain":
`mayfirst.cx`. A stub domain is a domain name that should not be resolved via
the public DNS system but instead should be resolved via the provided IP
addresses for the given domain.  

These provided IP addresses are the addresses of our `internal` authoritative
DNS servers, which listens on our `10.0.0.0/24` network that is only accessible
withint our cabinet, and only resolves to IP addresses on that internal network.

The stub domain allows us to manage host services that can only be accessible
from within our network (like databases).

### Authoritative servers

Our `authoritative` DNS servers, which run `knot`, only answer queries for DNS
lookups for domain names that we are the authority for: i.e. domain names
entered into our control panel or ansible playbook.

Some of our authoritave name servers are `public` - these are all located outside
of our cabinet (to avoid amplification attacks that could take down our
upstream). These exclusively get their zone information from the control panel.

Some of our authoritative name servers are `internal` - these are located inside
our network and only listen on the 10.0.0.0/24 network to resolve internal
queries. These servers get their zone information from our playbook (all
servers with an internal IP address is automatically entered into the zone file
by ansible).

In addition, some servers are `origin` servers - meaning they receive updates
directly (e.g. `nsstage001`, which receives updates exlusively from our control
panel). Others are `copy` servers, meaning they receive an rsync copy from the
`origin` servers, which ensures they are in sync.

The `origin` servers run systemd timers to regular copy records to the `copy`
servers to which they are configured to copy their zone files.

In contrast, both of our `internal` servers, are `static`, meaning we don't
bother copying from one to the other. These DNS records are automatically
generated from ansible (any host with an assigned IP address in the correct
range gets a record, additionally we have the `knot_bootstrap_internal`
variable in our `hosts.yml` file for extras. When making a change, we  have to
remember to push changes to both servers.
