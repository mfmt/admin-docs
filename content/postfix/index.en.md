+++
date = 2020-01-04T13:53:34Z
title = "Postfix"
categories = ["mail" ]
+++

## Overview

[Postfix](https://postfix.org) sends and delivers our email.

We use a central database of email accounts shared by all servers to ensure
that all servers are properly routing our email. Each postfix server uses a
combination of `virtual_alias_domains`, `virtual_alias_maps`, and `alias_maps`
(the mailstores) pointing to our database to keep track of users. 

We have several classes of postfix servers:

* **MX:** The [mx servers](/mx) accept incoming email destined for an address for
  which we will take responsibility and relays the email to the right final
  destination (either within our outside our infrastructure). 

    A domain will send email to us if it has an MX record pointing to our one of
    our servers.

    Typically a domain will designate two or more MX servers to receive email, in
    case one is down. The record with the lowest "weight" will be chosen first.

    We operate three MX servers:

    * a.mx.mayfirst.org weight: 10: primary and sole MX server under normal
      conditions, located in the cabinet

    * b.mx.mayfirst.org weight: 20: secondary MX server. Weight of 20 could be
      changed to 10 for load balancing if primary server is overloaded, located in
      cabinet

    * c.mx.mayfirst.org weight: 30, secondary MX server. Weight of 30. Located
      outside of cabinet, will be used if we need to shutdown direct access to
      cabinet in case of attack.

    Incoming email messages are put through a series of lightweight checks to
    reject obvious spammers.

    Note: while it would be better to do all checks and reject email at this
    stage, some checks (like virus checking) are resource intensive so we do
    them via the mailfilter servers. For more info on the pro's and con's, see
    the [Postfix pro's and con's of before-queue content
    filtering](http://www.postfix.org/SMTPD_PROXY_README.html#pros_cons).
    
    Next, they are sent to the restricted queue of the mailfilter server. After
    scanning and tagging, the internal messages are sent directly to the
    mailstore and the externally destined messages are forwarded to the
    restricted queue of the mailrelay server, which drops messages over the
    spam thresshold.

* **Mailfiter**: The mailfilter servers are designed to do the deep filtering
  of mail content to discard viruses and add spam scores. Once filtered,
  messages are relayed along to the appropriate next hop server. 

    The mailfilter servers first run all messages through clamsmtp and then (if
    it hasn't been silently disared) through spampd.

* **Mailstore**: The servers are the final destination for email
  addresses that are handled by May First. The mailstore servers also run
  [dovecot](/dovecot) so members can access their email using IMAP
  or POP.
    
* **CF**: CF, or client facing servers, accept email our members want to relay
  to others. Typically, members configure their email clients (mobile or
  destop) to point to the address `mail.mayfirst.org` which corresponds to a CF
  postfix server. The CF servers also run Dovecot which proxies any IMAP or POP
  connections to the appropriate mailstore server.

    Postfix is configured to authenticate requests to relay email via Dovecot.

    The CF servers send all local email to the mailfilter and external email to
    the bulk relay's individual instance.

* **Webstore**: Many web sites depend on the ability to send email. Our
  webstore servers run postfix in a way that automatically relays all email to
  the restricted instance of the mailfilter server which will forward it on to
  the restricted instance of the mailrelay server. If you are running a web app
  that is *supposed* to send bulk email (e.g. a CiviCRM instance) you can
  configure the instance to send directly to the bulk server.

* **Bulk**: The bulk servers accept email via the submission port (for
  authenticated users) or via port 25 via a list of specified IP addresses that
  are allowed to submit email (our web servers).

    The job of the bulk email servers are to route internally destined mail to
    our mailfilter servers to be scanned prior to internal delivery AND to send
    the rest of the externally destined email messages directly to our relay
    servers. We don't have the capacity or speed to scan bulk email destined
    for external addresses.

* **Relay**: Relay servers send email from inside our network to the rest of
  the world. They accept any email message sent from an allowed IP address. The
  relay servers run multiple instances of postfix to handle different kinds of
  relayed email:

   * *Bulk:* There are multiple bulk relay instances for each major email
     provider (such as Gmail, Yahoo, Microsoft, etc). We rate limit the
     relaying of such mail (one message per second or every two seconds) to
     reduce the chance that the receiving email server will block us. By
     running multiple instances per provider, we can send messages in parallel
     so they still go out quickly. The relayer instance only accepts email from
     the bulk instance.

   * *Individual:* There are several individual relay instances designed to
     relay email sent via our client facing (cf) servers. These servers are not
     supposed to send bulk messages so should have a more stable and solid
     reputation to improve deliverability.

   * *Restricted:* The restricted relays receive messages sent by the web
     servers and email sent by the MX servers that are being forwarded to an
     external addresses. To avoid forwarding spam and viruses, or to forward
     email sent by a compromised web site, messages with unacceptable spam
     scores are silently dropped.

## Diagrams

### Incoming email

All incoming email gets filtered. Internal email is delivered and external
email is relayed but via the restricted bulk relay so we hopefully drop most
forwarded spam messages.

![Incoming email](postfix.incoming.png)

### Relayed email

Relayed email is more complicated because we treat individual and bulk mail
differently.

![Relayed email](postfix.relay.png)

### TLS

For the most part, with incoming and outgoing email we offer tls but don't
require it (per Internet standards). However, for internal email we require tls
so we can relay email between colocation centers safely.

The only exception is sending bulk mail between web servers and our bulk mail
server which allows unauthenticated and un-encrypted email for legacy reasons.

![Relayed email](postfix.tls.png)
