+++
date = 2022-05-14T13:53:34Z
title = "Authentication Milter"
categories = ["mail" ]
+++

## Overview

Our MX servers run a mail filter (milter) on incoming email that runs the
alphabet soup of checks - DKIM, SPF, DMARC, etc.

For some checks (SPF, DKIM) it's responsible for adding headers indicating
whether the message passed those checks. For others (e.g. DMARC) it is
responsible for rejecting the message if it fails to pass.

We originally used the debian packaged `opendmarc` and `opendkim`, but the
upstream packages are not well maintained and we noticed some incoming email
messages caused opendmarc to crash, so we have switched to
[authentication_milter](https://github.com/fastmail/authentication_milter).

Unfortunately, it is [not in debian](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1036235) and installing it is a bit painful.

## Creating missing Debian perl modules

The recommended way of installing the package is via cpan (e.g. cpanm
Mail::Milter::Authentication).

But, first we install as many dependencies as we can via debian (and also libssl-dev
which is required to build the openssl packages.

The (some? not sure if this is all) perl modules not packaged in debian (or
with versions that are too old) are:

  * Lock::File
  * HTTP::Tiny::Paranoid
  * Alien::Build
  * Alien::Build::Plugin::Download::Gitlab
  * Mozilla::CA
  * Alien::Libxml2
  * Mock::Config
  * Mail::BIMI
  * Mail::DataFeed::Abusix
  * Mail::Milter::Authentication

