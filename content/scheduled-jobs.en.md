+++
date = 2024-12-10T03:53:34Z
title = "Scheduled Jobs in the control panel"
categories = ["red", "control-panel"]
+++

User are able to create scheduled jobs in the control panel.

These can be set to run on a given interval or "forever."

All jobs are translated into systemd service and timer files, which are saved in the site's `/.config/systemd/user` directory on the weborigin.

Users, when ssh'ed into the server, can manipulate them via `systemctl` provided they prefix the command with `--user`, e.g. `systemctl --user status red-item-1234.service`.

When logged in as root, you can't do that. Even if you `su` to the user, you still can't do that. Instead you have to invoke a complicated set of arguments along the lines of: `systemctl --user --machine <user>@.host status red-item-1234.service`.

To make things easier, a helper script is available that can be invoked simply as: `mf-systemctl status 1234` - where "1234" is the site id. If there is more than one service, you will be prompted to select the one you want.
