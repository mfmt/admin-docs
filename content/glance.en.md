+++
date = 2020-12-11T13:53:34Z
title = "Glance"
categories = ["logging", "alerts"]
+++

[glance](https://code.mayfirst.org/mfmt/glance) is a dead simple php script that
queries our [elasticsearch](/elasticsearch) database to see how many critical,
warning and info alerts have been sent in the last 10 minutes and the last
hour.

It's publicly available via https://glance.mayfirst.org/.

The goal of the site is to provide a quick, easy to access confirmation about
whether any problems we have received alerts about are resolved or still
active.

Additionally, pigeon sends a heartbeat message every 10 minutes, and glance
lists any servers that have not sent a heartbeat, so we can know if the reason
there are not alerts is because pigeon has stopped running. 

glance is authorized via a special read-only username configured manually via
kibana.
