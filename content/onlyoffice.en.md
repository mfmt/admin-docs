+++
date = 2024-03-24T03:53:34Z
title = "Only Office"
categories = ["nextcloud"]
+++

[Onlyoffice](https://www.onlyoffice.com/) is a web based application providing
office (Document, Spreadsheet, etc) like functionality. It is integrated into
our [Nextcloud](/nextcloud) installation.

We specifically install our own version of OnlyOffice provided via
[infrared](https://0xacab.org/infrared/platform_wg/docker-onlyoffice-documentserver).
This repo includes a docker and docker compose file. A few important considerations:

 * The docker compose file includes an nginx container, but we ignore it and
   provide nginx via the host, which is configured via
   `/etc/nginx/conf.d/proxy-https-to-http.conf`
 * You can control both the 0xacab and upstream git versions via ansible variables
   in the `onlyoffice001.mayfirst.org.yml` file.
 * When changing the git versions, *you must pass the --secrets argument to
   sower/ansible* because after updating git, we replace the JWT Secret variable in
   the `/.env` file and we can only do that with `--secrets`. The JWT Secret corresponds
   to the security key set in the Nextcloud admin interface for the Only Office app.

Our ansible scripts setup everything but it requires a manual intervention to
launch the containers. Change into the
`/usr/local/src/docker-onlyoffice-documentserver` directory and run:

    docker-compose up -d

## Status

When you access the Only Office URL you'll get an error (by design). But, if
you check /healthcheck (e.g. https://oo.mayfirst.org/healthcheck) you should
get a single word: true if it is working.

You can test via curl before going live using the `--resolve` argument:

`curl --resolve oo.mayfirst.org:443:<ip> https://oo.mayfirst.org/healthcheck`

Additionally, the IP lookup for the onlyoffice domain name is checked by your
web browser, not the server. So, assuming your browser is configured to
consulut your `/etc/hosts` file (not the Firefox default), you can test a new
Only Office server by updating that file and trying to open a document via Only
Office. You can tail `/var/log/nginx/access.log` to ensure you are accessing
the OO server you think you are.

## Upgrading

 * Consult the
   [docs](https://0xacab.org/infrared/platform_wg/docker-onlyoffice-documentserver),
   check for open tickets and ensure an new version has been tested.

 * Update the `onlyoffice_upstream_git_version` and `onlyoffice_git_version`
   variables in the `onlyoffice001` seed inventory yaml file. These are the variables that
   populate the `.env` file.

 * Run sower (be sure to pass the `--secrets` argument) - then check the
   `/.env` file to ensure it contains the values you expect.

 * Purge all the existing containers and volumes.
   * Shutdown the existing container cleanly to ensure all writes are saved back to nextcloud: `container_id=$( docker ps -aqf "name=onlyoffice-documentserver"); docker exec -t -i ${container_id} /usr/local/bin/documentserver-prepare4shutdown.sh`
   * Shutdown all docker containers: `docker-compose down`
   * Delete all containers:
     * Find them: containers: `docker ps -a`. 
     * Delete with: `docker rm <id>`. To find data
   * Delete all volumes:
     * Find them: `docker volume ls`. 
     * Delete with: `docker volume rm <name>`
   * Run the following in the folder containing docker-compose.yml (`/usr/local/src/docker-onlyoffice-documentserver`): `docker-compose up -d`
