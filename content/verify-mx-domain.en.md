+++
date = 2022-07-11T13:53:34Z
title = "Verify MX Domains"
categories = ["mail" ]
+++

## The problem

Any member is free to add a new domain to the DNS section of their control
panel provided no other member already has a domain name record matching it (we
use the [public suffix domain list](https://publicsuffix.org/) to figure out
the difference between a root domain and a subdomain). 

This policy means members could engage in sketchy behavior, like adding
`gmail.com` as their DNS record and then adding `maria@gmail.com` as their own
email address.

Under normal circumstances, no message destined for a gmail.com address should
ever enter our system (the sending server would direct messages with those
domains to the appropriate MX servers, not to us).

However, if one member's email address is `maria@example.org`, which does come
to May First, and they configure a forwarding address so all email sent to
`maria@example.org` forwards to `maria@gmail.com`, then we become responsible
for relaying that email to `gmail.com.`  And now, the sketchy member who added
`maria@gmail.com` to their control panel might fool our network into delivering
email that should go to Google to their own mailbox.

## The answer

To avoid this situation, all email domains must be verified as delivering to
May First before they are added to our routing tables.

### Adding an MX domain

Whenever a new domain is added to our control panel as an MX record, such as
`example.org`: 

 * the control panel flags the domain as unverified,
 * the domain is configured on our MX servers:
   * the domain is added to our MX servers so email from the domain will be
     accepted at the MX level (but not the routing table level, so it will
     never leave the initial MX server).
   * the email address `mayfirst-verify-mx-domain@example.org` is configured
     to deliver to our `verify-mx-domain` script which extracts the contents of
     the subject line of each email it receives and posts that string to our control
     panel. 
   * any mail sent to the domain but to a different email address is deferred.
 * the control panel adds a record in the pending domain verifications table
   with a random code
 * the control panel triggers an email with that code in the subject line to
   `mayfirst-verify-mx-domain@example.org`
 * when the control panel gets a post with that code, it updates the status of
   the domain name to verified
 * once a domain is verified, it is included in all routing tables.

 ### Maintenance

A maintenance process runs on all mx servers scanning the logs for any email that 
was deferred because it has not yet been verified. Whenever it detects one, it
triggers the control panel to send another probe email. This ensures that if the
initial probe email doesn't make it (e.g. the old MX records is still cached when
the control panel first sends out the probe), that the domain will still be verified
in a timely manner as soon as the TTL does expire.

Evey 24 hours, a test email is sent to all verified domains to see if they are
still with us. If a domain goes 2 days without a verification it is changed to
unverified.
