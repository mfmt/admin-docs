+++
date = 2020-10-14T13:53:34Z
title = "Nextcloud"
categories = ["nextcloud"]
+++

[Nextcloud](https://nextcloud.com) is a file, calendar, contact sharing web application (and much much more). 

May First administers a [Nextcloud instance shared by all
members](https://share.mayfirst.org/) - any user created in the control panel
with access granted to Nextcloud can login to this shared instance. In
addition, we can also can spin up instances dedicated to specific members.

## System design

There are five main components to a Nextcloud installation:

 * The database: We use a Postgres server running on a dedicated virtual
   machine
 * The web server: We run one nginx instance on the docker KVM guest that
   serves all docker instances
 * The PHP server: We run separate php-fpm docker containers for each Nextcloud
   instance
 * The redis server (key/value database to speed things up): We run a separate
   redis docker container for each Nextcloud instance.
 * The files: all files are served from the KVM guest hosting the docker
   instance and allocated to the docker container via bind mounting.

## Setting up an instance

 * Ensture the base image is installed: `docker images` (you should see
   my-buster). If not, run `mf-docker-create-base-image`

 * Choose a user for the instance (which will be referred to below as $USER).
   This user becomes a label used throughout the process: the name of the
   database, name of the database user, name of the logical volume, etc, so,
   *please* use all lower case and restrict to *just* the letters a-z - no
   dashes, underscores, punctuation or funny business! 

*  Allocate a logical volume for this instance: `lvcreate --size 10G --name
   $USER vg_nextcloud001_0` 

 * Create a filesystem on it (`mkfs -t ext4 /dev/mapper/vg0-$USER`).  If you
   expect it to grow behond 1TB, make it an xfs partition. 

 * Create a directory called `/srv/nextcloud/$USER`, add the partition to
   `/etc/fstab`, and mount the parition.

 * Fill out a nextcloud stanza on the nextcloud KVM guest yml file you want to
   use. Obviously, for the user field choose the user you created in the
   earlier step.

   In this step, you'll need to choose a domain name (be sure to create a DNS
   record pointing to the web proxy you will use before continuing). 

   And lastly, you'll also need to add `nginx_proxy` and `keystore` stanzas.
 
 * Apply the ansible changes. Be sure to apply to key001, webproxy001 as well
   as nextcloud001.

 * On the server, run `mf-nextcloud-install $USER` to complete the initial
   installation. This install script will spit out your admin password.

### Post install clean ups

Unfortunately, not everything is fully automated. After the install:

 * Edit the file `/srv/nextcloud/$USER/config/config.php` and change
   the `trusted_domains` parameter from localhost to the site domain.

 * Log in as admin, click Settings -> Basic Settings and change the background
   job to "cron" (the systemd service and timer should be set up
   automatically).

 * Enable and start the cron service:
    
    systemctl enable $USER-nextcloud-cron.timer
    systemctl start $USER-nextcloud-cron.timer

## Maintenance

### Using the Nextcloud command line tool `occ`

For convenience, there is an `mf-nextcloud-occ-wrapper` that provides easy
access to the `occ` script within a nextcloud instance. The `occ` script is the
Nextcloud command line client. You can run  useful commands like:

 * `mf-nextcloud-occ-wrapper $USER user:resetpassword admin`
 * `mf-nextcloud-occ-wrapper $USER maintenance:mode --on`
 * `mf-nextcloud-occ-wrapper $USER maintenance:mode --off`
 * `mf-nextcloud-occ-wrapper $USER list`

### Updating Nextcloud

When upgrading to a new version of Nextcloud, the images and directory on the
host will be automatically generated via ansible.

However, sometimes you may make changes to a Nextcloud version without bumping
the version (to pull in system updates or to add new nextcloud apps).

After changing the `nextcloud-download` script (in
`nextcloud/files/docker-nextcloud`) and pushing out the changes via ansible,
run the following on the nextcloud host (replace `PUT_VERSION_HERE` with a
version number like 18.4.2):

    export NEXTCLOUD_VERSION=PUT_VERSION_HERE
    cd /usr/local/src/docker-nextcloud
    # Install changes on nextcloud host
    ./nextcloud-download
    # Update nextcloud image to match
    docker build -t nextcloud_$NEXTCLOUD_VERSION --build-arg NEXTCLOUD_VERSION=$NEXTCLOUD_VERSION ./

## Upgrade

To upgrade an instance:

 * Manually build the new version on the server. Peforming this step makes
   everything go faster, and leads to less downtime during the upgrade. You can
   do that by following the directions above for updating Nextcloud
 * Alter the stanza for the site you want to upgrade in the server's yml file,
   change the nextcloud version number
 * Apply changes to the server. You can make it much faster by specifying
   `--tags nextcloud-upgrade` - that will only run the relevant tasks.
 * Manually edit the `/srv/nextcloud/$USER/config/config.php` file to change
   the `datadirectory` - the path has the Nextcloud version number embedded in it.
 * Run `docker-compose --file /srv/nextcloud/$USER/docker-compose.yml up -d` 
 * Run `mf-nextcloud-backup-database $USER`
 * Run `mf-nextcloud-occ-wraper $USER upgrade`
 * Log in as admin and check the basic settings. You may want to run:
  * mf-nextcloud-occ-wrapper $USER db:add-missing-indices
  * mf-nextcloud-occ-wrapper $USER db:add-missing-columns


