+++
date = 2021-05-03T13:53:34Z
title = "Jitsi Meet Server"
categories = ["jitsimeet"]
+++

## Overview 

The [jitsimeet server](https://github.com/jitsi/jitsi-meet) is the normally the
easiest component to install. We essentially follow the [quick
install](https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-quickstart)
instructions.

Most of the configuration changes we make are in the
`/etc/jitsi/meet/domain-config.js` file and we make this via `lineinfile`
changes. Our main goal with the changes from the defaults are to reduce
bandwidth and CPU pressure on the clients that connect.

We also copy in the `domain-interface-config.js` file, but so far we are not
making any modifications. This file *must* be re-copied after each jitsimeet
release (FIXME: we should automate somehow).

## Rebuilding with support for setParticipantVolume

We run an instance of jitsi meet that [supports
interpretation](/jitsimeet-simultaneous-interpretation). Fortunately, the core
change needed for this feature has [been
merged](https://github.com/jitsi/jitsi-meet/pull/9322)! So, nothing to do. 

## https 

Figuring out https is non-trivial. For most of our web services, we direct all
traffic via our web proxy servers, and then we handle https services via our
dedicated Lets Encrypt key servers.

However, with jitsimeet, directing all web traffic via  web proxy is
complicated since web services aren't the only services involved (there's also
the video bridge and the xmpp server that use the same domain name). So, we
don't use the web proxy method.

The jitsimeet debian package comes with a special script to generate the Let's
Encrypt certificate which we could use, however, it sets things up in a Linux
distrubtion agnostic fashion, rather then following debian standards (e.g. it
tries to download certbot rather then using the Debian version).

So, we use our own method that more closely hues to the Debian model and the
methods used else where in our ansible setup.

This process is made trickier by the fact that not only nginx, but also the
co-turn server need to share access to the generated certificate.

## Jibri and jigasi

Use of [jibri](/jitsimeet-jibri) (recording) and [jitsimeet-jigasi](/jigasi) (dial in) is optional. 

Jibri is intended to be installed on a different machine because it's a CPU hog
and we don't want recording of a meeting to interfere with the meeting itself.
If you want a jitsimeet install to integrate with a jibri install, set
`jitsimeet_jibri` to 'true` in the jitsimeet server's yml file.

Jigasi, on the other hand, is intended to be installed on the jitsimeet server
itself. To enable it, simply include `jigasi_enabled=true` in your yml file.

## Requring login

For the main May First instance, we do not require a login to create a meeting.
However, that's an option (and the PTP instance uses that option). You can set
the `jitsimeet_authenticated` variable to `true` to enable authentication.

Once enabled, you can run the following command to create or change usernames
and passwords:

    prosodyctl register <username> <meet.domain.org> <VerySecretPassword>
