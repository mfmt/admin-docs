+++
date = 2020-12-12T13:53:34Z
title = "Get Involved"
categories = ["orientation"]
weight = -50
+++

[May First Movement Technology](https://mayfirst.coop/) is a membership-based
organization with a mission to: engage in building movements by advancing the
strategic use and collective control of technology for local struggles, global
transformation, and emancipation without borders. 

Want to get involved?

May First has a Technology and Infrastructure Services team open to all members
of the organization.

If you are interested in joining, please login to our [discourse
instance](https://comment.mayfirst.org), view our [invitation to
participate](https://comment.mayfirst.org/t/invitation-to-participate-in-the-technology-infrastructure-and-services-team-invitacion-a-participar-en-el-equipo-de-infraestructura-y-servicios-tecnologicos/1954),
and [join the group](https://comment.mayfirst.org/g/TIAS).

Be sure to click the bell on the [Technology Infrastructure and
Services](https://comment.mayfirst.org/c/tech/15) category so you receive
alerts about future meetings.
