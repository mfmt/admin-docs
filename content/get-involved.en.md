+++
date = 2020-12-12T13:53:34Z
title = "Get Involved"
categories = ["orientation"]
weight = -50
+++

[May First Movement Technology](https://mayfirst.coop/) is a membership-based
organization with a mission to: engage in building movements by advancing the
strategic use and collective control of technology for local struggles, global
transformation, and emancipation without borders. 

Want to get involved? [TBD]
