+++
date = 2020-12-12T13:53:34Z
title = "Orientation"
categories = ["orientation"]
weight = 0
+++

The May First network is composed of a lot of moving parts that work together.
As the list of categories on the right demonstrate, it's a lot to take in!

This page is designed to ease you into things by providing a big picture.

## The code

All of our code lives at https://code.mayfirst.org/mfmt/.

That's great if you know what you are looking for, but for those who want a
high level summary, keep reading...

## From 1,000 feet up

With a few notable exceptions, May First's network ties together a number of
free software projects (the apache web server, postfix mail server, dovecot
mail server, nextcloud document and sharing server, etc) with our own custom
configurations, user databases and network architecture. 

Our custom "glue" that ties it all together is applied using
[ansible](/ansible), a configuration management system. With ansible, we write
a series of text files in [yaml
syntax](https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html)
that describe how we want our servers configured (mostly these files describe
which programs should be installed and how the default configuration files for
these programs should be changed for our purposes). Then, we run the ansible
command, our laptops connect over ssh to each server in our network, and those
configuration settings are applied to each server.

With ansible we can quickly setup new servers, ensure that all of our servers
are uniformaly configured, and provide an easily documented list of steps for
new administrators to learn how we setup our servers.

The code name for our set of ansible configuration files is **seed.** Out of
**seed** grows our network.

The second most significant piece of our infrastructure is our custom [control
panel](/flower), written in the Python web framework called
[django](https://www.djangoproject.com/), which is code-named **flower.** The
contol panel is our primary, user-facing tool, allowing members to manage their
own hosting resources. It provides both a user interface (the web site members
log into) and also a client/server component that communicates the member's
request to the appropriate server so the resource can be created.

If you are looking for a place to start, [seed](/ansible) and [flower](/flower)
are good choices.

## Other pieces

There are a lot of additional pieces that make up our infrastructure, but some
are bigger then others and some are more unusual then others.

In no particular order, here are some suggested entry points:

 * **Email:** Our email system is really complicated, but not particularly unusual.
   Starting with our [postfix](/postfix) page is a good idea - there are some
   useful diagrams to help guide you through the process. Also, keep in mind,
   all email-related information is stored in our [control panel](/flower)
   which synchronizes with our [ldap server](/ldap).

 * **Logging, Monitoring, Alerting and Instrusion Detection:** There are a lot
   of connected systems here as well. The heart of the system is
   [elasticsearch](/elasticsearch).  But, there is much more, as the
   [monitoring overview](/monitor) and [intrusion
   detection](/intrusion-detection) pages describes.

 * **Installing the operating system:** We exclusively run the [Debian GNU
   Linux operating system](https://debian.org/). If you are interested in
   learning how that happens, our [install pages](/categories/install) shows
   how we boot from the network, install to physical machines and use [kvm
   manager](/kvm-manager) to install and manage virtual servers on each
   physical server.
