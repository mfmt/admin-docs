+++
date = 2020-12-12T13:53:34Z
title = "Orientation"
categories = ["orientation"]
weight = 0
+++

The May First network is composed of a lot of moving parts that work together.
As the list of categories on the right demonstrate, it's a lot to take in!

This page is designed to ease you into things by providing a big picture.

## The code

All of our code lives at https://gitlab.com/mfmt/.

That's great if you know what you are looking for, but for those who want a
high level summary, keep reading...

## From 1,000 feet up

With a few notable exceptions, May First's network ties together a number of
free software projects (apache, postfix, dovecot, nextcloud, etc) with our own
custom configurations and architecture. 

Our custom "glue" that pulls it all together is applied using
[ansible](/ansible), a configuration management system. With ansible, we write
a series of text files in yaml syntax that describe how we want our servers
configured. When we run ansible, those configuration settings are applied.

The code name for our set of ansible configuration files is **seed.** Out of
**seed** grows our network.

The second most significant piece of our infrastructure is our custom [control
panel](/flower), written in django, which is code-named **flower.** Our contol
panel is the primary, user-facing tool, allowing members to manage their own
hosting resources, providing both a user interface and also a client/server
component that manages the creation of those resources on our servers.

If you are looking for a place to start, [seed](/ansible) and [flower](/flower)
are good options.

## Other pieces

There are a lot of additional pieces that make up our infrastructure, but some
are bigger then others and some are more unusual then others.

In no particular order, here are some suggested entry points:

 * **Email:** Our email system is really complicated, but not particularly unusual.
   Starting with our [postfix](/postfix) page is a good idea - there are some
   useful diagrams to help guide you through the process. Also, keep in mind,
   all email-related information is stored in our [control panel](/flower)
   which synchronized with our [ldap server](/ldap).

 * **Logging, Monitoring, and Alerting:** There are a lot of connected systems
   here as well. The heart of the system is [elasticsearch](/elasticsearch).
   But, there is much more, as the [monitoring overview](/monitor) page
   describes.
