+++
date = 2019-08-21T13:53:34Z
title = "Ansible - Initialize docker containers"
categories = ["ansible"]
weight = -25
+++

## Overview

The [`sower` command](/ansible-sower) can be used to build out a set of docker
containers that can mimic a real network of servers, allowing us to test and
develop our ansible playbook.

The docker images are used for dev and learning purposes only. They should not
be used to host any production services.

The script builds out docker images for convenience. It builds them in 
non-standard ways. In particular:

 * It builds our own base debian bullseye image so we don't have to blindly
   pull images from the Internet and run them on our machines.

 * It builds out docker images that run systemd on the inside. Most docker
   images either don't run an init program (they run the service directly) or
   they use a simpler init system (like dumb-init). These images are built
   using systemd so they more closely reflect the way our actual guests are
   run.

 * It install openssh in the image without re-creating the server keys. This
   means the private keys are embedded in the image. This is a big security
   problem for production sites, but a nice convenience for testing because it
   means you don't have to keep approving ssh keys every time you re-create
   containers.

## Steps

If you haven't already, ensure you have both docker.io and debootstrap packages
installed:

    apt install docker.io debootstrap

You need to copy your public ssh key to the docker directory and name it:
`id_rsa.pub`. This allows your key to be added to `/root/.ssh/authoried_keys`
thus allowing you to ssh in without any fuss.

Next, run the `./build-os-image bullseye` command, as root (or via sudo), from
the docker directory. This script will create a debian stable docker image so
we don't have to pull it off the Internet. It must be run as root.

Lastly, run the `sower docker:init` command.

It will:

 * Create a seed docker network.
 * Create a seed-base image.
 * Build out all containers used in the dev ansible environment.
 * Warn you if you need to add any entries to your `/etc/hosts` file. Please
   follow these directions! Putting the entries in your `/etc/hosts` file
   allows ansible to find them via their hostnames.

Assuming your user is in the docker group, you can run it as a non-privileged
user.

The following arguments control how it works

 * `--force`: force a rebuild of everything.
 * `--apt-proxy https://blah.proxy.org/debian`: use an apt proxy server
