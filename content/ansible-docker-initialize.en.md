+++
date = 2019-08-21T13:53:34Z
title = "Ansible - Initialize docker containers"
categories = ["ansible"]
weight = -25
+++

## Overview

The docker images are used for testing and learning purposes only.

The `initialize-docker-environment.py` script builds out a set of docker
containers based on the hosts.yml file specified (e.g.
inventories/dev/hosts.yml).

The script builds out docker images for convenience. It builds them in 
non-standard ways. In particular:

 * It builds our own base debian buster image so we don't have to blindly pull
   images from the Internet and run them on our machines.
 * It builds out docker images that run systemd on the inside. Most docker
   images either don't run an init program (they run the service directly) or
   they use a simpler init system (like dumb-init). These images are built
   using systemd so they more closely reflect the way our actual guests are
   run.
 * It install openssh in the image without re-creating the server keys. This
   means the private keys are embedded in the image. This is a big security
   problem for production sites, but a nice convenience for testing because it
   means you don't have to keep approving ssh keys every time you re-create
   containers.

## Steps

You will need to install a relatively modern version of docker (version 18 or
above). If you are running buster, the default debian version will do fine.

You also need to copy your public ssh key to a file (scripts/docker), called
id_rsa.pub.

Next, run the `./build-os-image` command, as root, from the scripts directory. 
This script will create a debian buster docker image so we don't have to pull
it off the Internet. It must be run as root.

Next, run the `./initialize-docker-environment.py ../inventories/dev/hosts.yml`
command from the scripts directory.

It will:

 * Create a seed docker network.
 * Create a seed-base image.
 * Build out all containers used in the testing ansible environment.
 * Warn you if you need to add any entries to your /etc/hosts file.

Assuming your user is in the docker group, you can run it as a non-privileged
user.

The following environment variables control how it works

 * SEED_FORCE=1: force a rebuild of everything.
 * SEED_APT_PROXY=https://blah.proxy.org/debian: use an apt proxy server
 * SEED_FLOWER_PATH=/home/jamie/projcts/mfpl/flower: When building the docker containers, mount the path
   over /usr/local/share/flower on the docker container. This makes it much
   easier to developer our flower control panel while testing on our seed
   docker images, since they all will be using the same code.
