+++
date = 2023-12-11T13:53:34Z
title = "Setting up a new shared MySQL (MariaDB) server"
categories = ["sql", "red", "control-panel"]
+++

The following steps should be taken to setup a new shared mysql server:

1. In ansible seed repo, edit `hosts.yml` to copy each line with `mailstore002` to
   your new name.
1. Execute `sower --live generate:mysql --parent hostxxx.mayfirst.org` to generate a new mysql yaml file.
1. Follow [regular kvm guest setup steps](/kvm-manager/#what-to-do-specifically)
1. Be sure you created the ssh key and enabled quotas in the step above! 
1. Update all weborigins to let them know about the mysql server so they can connect
   via proxysql: `sower --live playbook --tags proxysql-build-origins-file weborigin00[1234].mayfirst.org`
1. Update puppet file `modules/mayfirst/files/proxysql/proxysql-servers.yml` and add the information
   for the mysql server you are adding. Push to any mosh that needs to access this database (or just sign a tag).
