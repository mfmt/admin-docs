+++
date = 2022-10-19T13:53:34Z
title = "User Quotas"
categories = ["red", "flower", "control-panel"]
+++

Our control panel restricts disk usage by resource (e.g. a user login or
database).

## Databases

For databases, we simply use `du` to check database usage  via a cron job
and, if a database has gone over, we disable each database user with write
access to that database.

## Users

For email and web site disk usage, we use the ext4 quota sytem to set a quota
and depend on it to inform users when they have gone over.

### Enabling quotas

quotas are enabled automatically by the `sower --live generate:fstab` command
when operating on a weborigin or mailstore server.

## Keeping it all in sync

A cron job runs on every server with quota
(`/usr/local/share/red/node/sbin/update-disk-usage`) that checks disk usage and
copies it back to the contorl panel.
