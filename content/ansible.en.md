+++
date = 2019-08-21T13:53:34Z
title = "Ansible: server configuration system"
categories = ["ansible"]
weight = -100
+++

[Ansible](https://www.ansible.com/) is the server configuration automation tool
we use to ensure our servers are configured in a uniform way.

Our ansible setup involves two git repositories:

 * **[seed](https://code.mayfirst.org/mfmt/seed)** is the name of the git
   repository containing our roles and most of our ansible code. It also
   includes the `inventories/dev` directory, which allows you to create a minimal
   set of docker images that demonstrates how our ansible repo is designed to
   work.

 * in addition, we have a private repository containing the production
   inventory called **seed-mayfirst**. This repo is private because it contains
   sensitive information, like the IP addresses of our servers, server
   locations, and passwords.

For more information, see:

 * [seed](https://code.mayfirst.org/mfmt/seed) - the actual seed repo.
 * [hacking ansible](/ansible-hacking) - instructions for checking out and hacking on our ansible code.
 * [ansible vault](/ansible-vault) - how we store secrets in ansible.
 * [ansible adhoc](/ansible-adhoc) - Running ad-hoc commands on all servers.
