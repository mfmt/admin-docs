+++
date = 2025-01-18T03:53:34Z
title = "Instant Message and Chat via Prosody and Movim"
categories = ["chat"]
+++

We provide XMPP services via [prosody](https://prosody.im/). And we provide an
XMPP client via [movim](https://movim.eu/).

## Prosody

Prosody is installed via debian. It authenticates via [Cyrus
SASL](https://modules.prosody.im/mod_auth_cyrus.html), which is configured to
check `/etc/passwd` via PAM. The control panel simply uses `useradd` to add
users to the system in order to enable their ability to login to our prosody
server.

## Movim

Movim is installed via git.


### Initial configuration

After installing movim, run: `php daemon.php setAdmin <jid>` to set the admin jid.

Once set, the user that logs in with that jid can set administrative settings. We have the following set:

 * **Restrict suggestions** - Only suggest chatrooms, Communities and other contents that are available on the user XMPP server and related services
 * **Chat only** - Disable all the social feature (Communities, Blog…) and keep only the chat ones
 * **Disable the XMPP registration feature** - Remove the XMPP registration flow and buttons from the interface
 * **Whitelisted servers** - We only list mayfirst.org - you cannot use our movim to login to any other XMPP server.

I also manually uploaded the May First banner and configured the banner path.


### Upgrade
To upgrade Movim:

1. Become the movim-src user: `su - movim-src -s /bin/bash`
2. Change into the src directory: `cd /var/www/movim/web`
3. Run git pull: `git pull`
4. Run composer: `composer install`
5. Exit to root: `exit`
5. Become the www-data user: `su - www-data -s /bin/bash`
6. Change into the src directory: `cd /var/www/movim/web`
7. Run any database upgrade: `composer movim:migrate`

