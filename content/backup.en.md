+++
date = 2020-10-07T13:53:34Z
title = "Backup"
categories = ["backup"]
+++

We use both [backupninja](https://www.0xacab.org/riseuplabs/backupninja), which
provides a tool-agnostic framework for configuring backup jobs, and
[borgbackup](https://www.borgbackup.org/), which performs a network backup of
the data.

Via ansible, we can designate a server's `backup_profile` as either a `target`
(the backup server) or a `source` (any server that should specify directories
to backup).

When defining sources, you can specify a list of `remotes` (in case you want to
backup to more then one remote backup servers), and also a list of directories
to `include`.
