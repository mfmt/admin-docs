+++
date = 2020-10-07T13:53:34Z
title = "Backup"
categories = ["backup"]
+++

We use both [backupninja](https://www.0xacab.org/riseuplabs/backupninja), which
provides a tool-agnostic framework for configuring backup jobs, and
[borgbackup](https://www.borgbackup.org/), which performs a network backup of
the data.

Via ansible, we can designate a server's `backup_profile` as either a `target`
(the backup server) or a `source` (any server that should specify directories
to backup).

When defining sources, you can specify a list of `remotes` (in case you want to
backup to more then one remote backup servers), and also a list of directories
to `include`.

## Transitioning from puppet

At the moment, our main backup servers are managed by
puppet. Yet, our new ansible servers still need to backup to them.

To handle the transition:

 * Backup sources in ansible: When specifying a server that should be the
   source of a backup,, use the new, ansible way (.e.g. specify that
   `backup_profile` is set to source, and designate the target backup server
   and the directories to be backed up like you normally would.

 * For each source that you add, you need to add a setting in the old puppet
   configuration too, since you can't set it up in ansible.

   * Pick the right `.pp` file. Since we don't have a .pp file for all the new
     ansible virtual servers, instead choose the `.pp` file for the physical
     host. We will maintain both ansible `.yml` and dupcliate puppet `.pp`
     files files for all physical hosts during the transition.

   * Add an entry in the hosts `.pp` file along these lines:

```
if ( $::fqdn == "XXXXX.mayfirst.org" ) {
  mayfirst::m_backupninja::server::configure_node { "XXX003": }
}
```

   * Add the server's public key to the `$mfpl_ssh_public_keys` dictionary in
     the `globals.pp` file keyed to the server name as specified in the
     `configure_node` line.
