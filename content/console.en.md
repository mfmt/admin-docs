+++
date = 2020-03-13T13:53:34Z
title = "Accessing the console on virtual and physical servers"
categories = ["console"]
+++

Sometimes network access to a server is not available, or you need to perform
an operation before or during boot (like enter an encrypted passphrase).

During these times, it's useful to gain access to the server's console.

All servers, both bare metal and virtual servers, offer console access via ssh
through a different server.

## Virtual servers

Consoles on virtual servers can be accessed via a non-privileged user on the
bare metal server that provides the virtual server. If you operate a virtual
server, you can request access to this user by contacting support.

With access, you can access it via
`NAME-OF-VIRTUAL-SERVER@ADDRESS-OF-HOST-SERVER`. For example, if your virtual
server is named foo.mayfirst.org and it runs on metal-003.mayfirst.org, you
would access it via ssh using the address `foo@metal-003.mayfirst.org`.

Once you have ssh access, you can gain access to the console via the command:

    screen -x

You can detach from the console with: `ctl-d`

## Physical Hosts

Access to the console of physical hosts varies depending on the colocenter.

Each physical server's dot yaml file should include comments with instructions
on how to connect.

The following instructions are for all servers in our Telehouse location, which
are managed via our dedicated console server accessible via
`console.mayfirst.org`.

If a server is called `severo.mayfirst.org`, then you would access the console
via `ssh severo-console@console.mayfirst.org`.

### IPMI

If the host offers access via `ipmi` then you would run a serial console over
ipmi by executing the `./sol` helper script in the home directory.

### Serial

If the host uses a hardware serial port, you would connect via: cereal attach HOSTNAME, e.g.:

    cereal attach metal-003

### What about the console server?

The server that runs console.mayfirst.org is clr. To access the console of clr:
`ssh clr-console@wiwa.mayfirst.org` and run 'cereal attach clr'.


### Set up

See the [console admin](/console-admin) docs on how to setup your hosts via ansible.
