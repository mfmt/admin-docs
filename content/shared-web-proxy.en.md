+++
date = 2023-11-11T13:53:34Z
title = "Setting up a new shared web proxy"
categories = ["web", "red", "control-panel"]
+++

At the time of this writing, we have five active web proxies : webproxy006, webproxy005,
webproxy002, webproxy003, webproxy004 and multi001 and one inactive webproxy: webproxy001.

## Adding

1. In ansible seed inventory repo, edit `hosts.yml` and add the new host in all places an existing host exists.
1. Execute `sower --live generate:weborigin --parent hostxxx.mayfirst.org` to generate a new weborigin yaml file.
1. Follow [regular kvm guest setup steps](/kvm-manager/#what-to-do-specifically)

Note: The next steps have to happen close together. After you run the first step, changes to any web configuration
in the control panel will fail until you  have completed the remaining steps.

1. Run sower playbook with `--tags red` against `red001`
1. From the key server, copy all certs and keys to the new server:
`mf-cert-distribute --send-to-server <webproxy> --copy-only --all-certs`
*Warning*: No output, and it takes a while! You can track progress by counting directories on the webproxy in `/etc/ssl/mayfirst`
1. On the control panel UI server, copy out all the ngninx configuratins (without a restart of nginx):
    su - red -s /bin/bash
    /usr/local/share/red/ui/sbin/red-nginx-distribute --all --send-to-server <webproxy>
1. Manually restart nginx on the new web proxy server.
1. Update nftables on all weborigin servers: `sower --live playbook --tags nftables weborigin00[1234567].mayfirst.org`

## Removing

1. On the web proxy, run `mf-web-sites-resolving-here` to ensure no existing web sites are still resolving to this web proxy.
1. In ansible seed inventory repo, edit `hosts.yml` and update the `m_web_proxy_servers` variable to remove the new host.
1. Run sower playbook with `--secrets --tags red` against `red001`
1. Run sower playbook with `--tags keystore` against 'key001`
1. On the key server, remove from the sqlite file cache (replace "NNN" with the actual number):
`sqlite3 /var/lib/mf-cert-distribute/dist.db "DELETE FROM file_sync WHERE name LIKE 'webproxyNNN%'"`
