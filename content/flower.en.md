+++
date = 2019-08-21T13:53:34Z
title = "Flower: The May First control panel"
categories = ["flower", "control-panel"]
+++

The [May First control panel](https://code.mayfirst.org/mfmt/flower.git), code named
"flower", is written in python using the [django](https://django.org/)
framework.

The project README file covers that basics, along with instructions on how to
launch it and hack on it.

However, in order to fully develop in flower, it's useful to run it within the
ansible setup so you can properly ensure that the resources you create in the
control panel are properly created on the target petal servers.

When the ansible containers are created by default, the flower code will be
downloaded onto each petal server. This setup makes hacking tough because you
have to edit the files on the petal servers, test, then copy down to your git
repo to commit.

As an alternative, when running the `./initialize-docker-environment`, pass the
`SEED_FLOWER_PATH` environment variable set to the path in which you checked
out your flower code base on your host machine. With this environment variable
set, instead of downloading flower, your flower directory will be mounted onto
every docker container. This way, edits you make on your host computer will
immediately be visible on each docker container.
