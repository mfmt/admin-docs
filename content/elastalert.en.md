+++
date = 2019-08-13T13:53:34Z
title = "Elastalert"
categories = ["logging", "alerts", "intrusion-detection"]
+++

[Elastalert](https://github.com/Yelp/elastalert) provides monitoring and
intrusion detection services by querying our [elasticsearch](/elasticsearch)
database for indications of problems and then issuing an alert.

Elastalert comes with built-in alert types (we use email to notify admins of
urgent issues).

One alert,
[command](https://elastalert.readthedocs.io/en/latest/ruletypes.html#command),
simply runs a command. We use this alert type to execute `bruce ban` which
triggers an IP address to be banned via the [bruce](/bruce) IP banning tool
(see the [intrusion detection(/intrusion-detection) page for a full explanation
of how all the pieces work together].) 

## Rules

Elastalert has [good
documentation](https://elastalert.readthedocs.io/en/latest/) which explains how
rules and alerts are created.

 * rule: rules are queries against elasticsearch that either return a match or don't
 * alerts: alerts are what should happen if a rule returns a match.

### Working with Pigeon

[pigeon](/pigeon) is our problem detection software, which runs on all servers
and detects conditions we should be warned about and sends that warning to
elasticsearch via journald.

We have two simple elastalert rules that trigger emails when pigeon warns us of
either a critical or warning priority alert.

Because debugging elastalert conditions and rules is complicated, all of the
logic for determining if a condition deserves an alert is contained within
pigeon - the elastalert rules simply look for the pigeon flag and alerts.

#### Intrusion detection strategy

Our alert system can also trigger [bruce](/bruce), our IP banning software, to ban an IP.

At the risk of stating the obvious:

1. We don't want to ban users who simply got their password wrong or are using
   the wrong username or otherwise making a mistake that requires them to try
   several times in a row before they realize what they are doing wrong.

   This scenario looks like several failures in a short period of time.

2. We do want to ban robots designed to probe and test our system. The robots
   are typically designed to evade intrusion detection systems by make requests
   periodically over a longer time frame.  ### Testing rules

So, good intrusion detection systems will look over a long period of time. If
you want to ban on 10 failures, it's better to say 10 failures of 6 hours then
10 failures of 10 minutes.  

### Writing and debugging rules

It can be tricky to debug your rule. Be sure to carefully review the
[elastalert docs](https://elastalert.readthedocs.io/en/latest/) for all the ins
and outs, but briefly, there are two tips:

 * Use the `elastalert-test-rule` command as your first sanity check, e.g.:

    elastalert-test-rule --config /etc/elastalert/elastalert.yml --start 2019-11-06 /etc/elastalert/rules/elastaban-nginx.yml

 * Shutdown the elastalert service, and run it manually (as the elastalert user of course):

		elastalert --config /etc/elastalert/elastalert.yml --verbose

## Location

While [es](/elasticsearch) holds all our critical data and should be housed in a
protected environment, elastalert can run on a remote server and query es over
the Internet so it can more effectively alert us to problems.

## Index maintenance

elastalert expects indices to be created. This task is handled via ansible by
running the `elastalert-create-index` command that ships with elastalert.
Ansible runs this command on the server with `elastalert_profile` set to
"index" (meaning it runs on the elasticsearch server itself, not the server
running elastalert). It's indepotent. Once these indices are created, they are
used forever.

When installing for the first time, we should let ansible run the
`elastalert-create-index` command, but then, we have to run our own
`mf-elastic-elastalert-fix-indices` script. Sigh. By default, the elastalert
indices will expect a replica that we don't have. This script specifies 0
replicates so they can remain in the "green" state.

Finally, we have a timer that runs a script to purge all data from all
elastalert indices that are older then the specified number of days. This
ensures privacy.
