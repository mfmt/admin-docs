+++
date = 2021-09-09T13:53:34Z
title = "Jitsi Meet Simultaneous Interpretation"
categories = ["jitsimeet"]
+++

## Overview

Jitsi Meet has an [extensive
API](https://jitsi.github.io/handbook/docs/dev-guide/dev-guide-start),
including one designed to work via an
[iframe](https://jitsi.github.io/handbook/docs/dev-guide/dev-guide-iframe).

In other words, you include your main Jitsi Meet web page via an iframe, and then write
javascript code to control how it works.

## JSI

We have written a simple wrapper called [JSI](https://gitlab.com/mfmt/jsi)
using the iframe API to provide a intrepretation slider.

It's installed on the same server running Jitsi Meet and enabled via a separate
nginx configuration file.
