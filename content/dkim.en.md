+++
date = 2022-05-18T13:53:34Z
title = "DKIM Signing of outgoing email"
categories = ["mail"]
+++

[Domain Key Identified Mail
(DKIM)](https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail) is a method
for digitally (and transparently) signing outgoing email in a way that
indicates it was authentically sent by the person who claims to have sent the
message. 

The purpose of DKIM is to stop fraudulent email messages by preventing people
from sending email claiming to be from email addresses to which they don't in
fact have access.

## More specifically 

DKIM requires the generation of a public/private key pair for each domain name
that sends email via May First. 

The **private** part of the key exists on our outgoing [relay](/relay) servers
so they can use it to sign messages. The **public** part of the key is
published via our DNS servers so it can be publicly looked up to verify the
authenticity of an email message signed by the private key.

## The dance

To get all the parts in the right place, we need cooperation between the May
First control panel (where users can indicate they want a DKIM key generated),
our DNS servers which are in charge of publishing the public part of the key,
and our [relay](/relay) mail servers which are in charge of signing email using
the private part.

We handle it by:

 1. The control panel generates a DKIM DNS records
 2. The public/private keypais is generated on the staging name server.
   * The private key is stored on the staging server and rsync'ed to all mailrelay servers.
   * The public part is used to generate a DNS record, which is sync'ed to all authoritative name servers.
