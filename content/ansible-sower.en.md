+++
date = 2021-11-10T13:53:34Z
title = "Ansible - sower helper command"
categories = ["ansible"]
weight = -25
+++

## Overview

Our ansible scripts and our seed inventory combine a lot of useful information
about our infrastructure. The `sower` command is designed to easily parse the
information and manipulate it.

## Some details

### Setup

To effectively use sower, you should ensure that the path to the executable is
in your `$PATH` environment variable.

If you have access to our live inventory, then you should also set an environment variable:

    SEED_LIVE_DIR=/path/to/seed/inventory/directory

This environment variable is not necessary when operating in dev mode.

You also will need a few python libraries, which you can get via:

    apt install python3-dnspython

### Getting help

Since the `sower` command is under constant development, the best way to learn about what is possible is to run it with `--help`. At the moment it returns:

    usage: sower [-h] [--live] {playbook,ip:reconcile,ip:report,ip:next,docker:init,ls,red:sync} ...

    Run ansible code for May First.

    positional arguments:
      {playbook,ip:reconcile,ip:report,ip:next,docker:init,ls,red:sync}
        playbook            Run the live playbook
        ip:reconcile        Ensure all pingable IPs are assigned
        ip:report           Print report on full IP usage
        ip:next             Print assigned IP along with suggest next IP address.
        docker:init         Initialize development docker environment
        ls                  List servers matching the given criteria
        red:sync            Sync inventory with red control panel.

    optional arguments:
      -h, --help            show this help message and exit
      --live                By default, run on dev inventory, specify --live to run on live inventory

To get more information on a sub-command:

    sower playbook --help

## Example commands

### Running the playbook

The most common command is to run `ansible-playbook`. The `sower` commands
helps by taking care of al the default arguments so you can type a shorter
command. It also prints the `ansible-playbook` command that it is executing so
you don't lose touch with the underlying tool.

Since `sower` operates on the dev inventory by default, you can run a playbook against dev with:

    sower playbook

To run against just one host:

    sower playbook webproxy001.mayfirst.dev

or two:

    sower playbook webproxy001.mayfirst.dev,weborigin001.mayfirst.dev

To only execute one role:

    sower playbook --tags nginx-proxy webproxy001.mayfirst.dev,weborigin001.mayfirst.dev

When running against the live inventory, there are some more things you have to add, starting with the `--live` argument:

    sower --live playbook webproxy001.mayfirst.dev

Our playbooks reference a lot of secrets. If you don't specify `--secrets` when
running on the live inventory, those tasks will be skipped. If you want to
include them, type the following (and enter the password at the prompt):

    sower --live playbook --secrets webproxy001.mayfirst.dev

### Listing servers

The `sower` command is a great way to generate lists of servers using the `ls` subcommand:

    sower ls


You can specify patterns just like with ansible. Ansible patterns match against
either host names or groups.

    sower ls 'all:!exernal'

That means match all servers but exclude those in the external host group.


By default you get a list of server names. But you can also list attributes of servers with `--display`:

    sower ls 'all:!external' --display kvm.host,network.0.networks.0.address

Thatt command prints both the name of the guest's kvm.host as well as it's first network address.
