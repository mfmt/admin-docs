+++
date = 2019-08-13T13:53:34Z
title = "Kibana: Reports and Queries of Elastic Search"
categories = ["logging"]
+++

[Kibana](https://www.elastic.co/products/kibana) provides a user interface for
querying the es server.

kibana should be accessible via: https://report.mayfirst.org:5601/

If you are in dev mode and you get a tls warning, be sure to import the
certificate authority saved in inventory/testing/ca.

The password for kibana should eventually be placed in [keyringer](keyringer) but
for testing purposes a user with the username `mayfirst` and password `mayfirst` is
created and should grant you access.

When you login for the first time:

 * click the discover icon in the top left corner (compass icon)
 * you should be prompted to create an index
 * Create one with: `journalbeat-*` and click Next step.
 * For time filter field name, choose event.created 
 * Now, click the compass again and either broswe the results or search
