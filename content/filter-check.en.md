+++
date = 2022-05-16T13:53:34Z
title = "Filter Check"
categories = ["logging", "alerts", "mail", "intrusion-detection"]
+++

[filter-check](https://code.mayfirst.org/mfmt/filter-check) notifies us if
email sent via our servers is landing in the spam box of the three major
providers - Google, Microsoft or Yahoo.

We have created email accounts on all three providers. filter-check iterates
over all of our relay IP addresses and over all three providers to send an
email from each IP address to each provider. Then, it waits a bit and tries to
check for that message via IMAP. If the messages is in the Spam box or doesn't
arrive at all, it raises an alert.

filter-check is run via [pigeon](/pigeon). Pigeon manages the timing and takes
care of iterating over all the available postfix instances (this check is run
on each of our [mailrelay servers](/relay)).

If we get an alert, [follow these instructions](/alert-response#filtter_check).

