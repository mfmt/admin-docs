+++
date = 2024-06-19T13:53:34Z
title = "Outreach: May First's internal CiviCRM database"
categories = ["civicrm"]
+++

May First maintains our own CiviCRM installation via a standard hosting order
using the address `outreach.mayfirst.org`.

It's running Drupal 7 (probably to be switched to CiviCRM Standalone
eventually).

## Code base

The "sites/all" directory is kept in [git](https://code.mayfirst.org/mfmt/outreach).

We keep a few Drupal modules, but mostly the code we maintain is in the
"mayfirst" CiviCRM extension. This code provides our custom membership
workflow, displays, scheduled jobs, reports and other functionality.

## Tests

Our custom CiviCRM extension has a tests directory that can be used to ensure
our custom membership functionality is working after an upgrade.

To setup your system to run tests, take these one time steps:

1. Ensure you have a working local copy of the Outreach CiviCRM database.
2. [Download the `cv` tool](https://github.com/civicrm/cv).
3. Create your `cv.json` file ([see setup](https://docs.civicrm.org/dev/en/latest/testing/), mostly you have to edit the `TEST_DB_DSN` value, adding a username and password with root access to your database and a database name that will be used for the tests).
4. Install phpunit.
5. Change into the mayfirst extension's directory
6. For each file in `tests/phpunit/Civi/Mayfirst/` (except MayfirstBase.php) run: `phpunit /path/to/test`.

## Upgrade

To upgrade:

1. Delete the `civicrm` directory from the modules directory.
2. Download [the upgraded version](https://civicrm.org/download) and unpack it.
3. Download the localization files (same link as download link). Unpack the localization files. Move the '`l10n` directory into the main civicrm module directory and copy the files in the `sql` directory to the  `sql` directory in the main civicrm module directory.
4. Run `cv upgrade:db`
5. Test locally
6. Commit changes and push to live
7. Run `cv upgrad:db` on live site.

