+++
date = 2021-01-01T13:53:34Z
title = "signal-cli: command line signal client used to notify administrators"
categories = ["alerts"]
+++

[signal-cli](https://github.com/AsamK/signal-cli) is a command line
[signal](https://signal.org) client used by our [alerter](/alerter) script for
sending messages to administrators when something has gone wrong.

We install the [pre-packaged debian
binaries](https://packaging.gitlab.io/signal-cli/installation/standalone/).

After installation, manual setup is required for a new signal phone number.

## Primary Host

The phone number used by `signal-cli` is specified in the inventory using the
`signal_cli_number` variable. It should be in the format: +15555555555. In the
examples below we use this number as the example number.

The first host `signal-cli` is enabled on becomes the primary host. All other
hosts are secondary hosts.

On the primary host:

 * Become the signal-cli user: `su - signal-cli -s /bin/bash`
 * Register the phone number: `signal-cli --config /var/lib/signal -u +15555555555 register`
 * Check the sms inbox for that phone number to retrieve the code.
 * Verify the code: `signal-cli --config /var/lib/signal -u +15555555555 verify xxx-xxx`

But... if you want to join v2 groups you have to update your profile (and even
if you don't want to join Signal groups maybe you want to update it?)

    signal-cli --config /var/lib/signal -u +15555555555 updateProfile --name "foo" --avatar /var/lib/signal-cli/pic.png

Done! Now you can start the service (as root): `systemctl start signal-cli`

## Secondary hosts

 When you install signal-cli on a second host, the following steps are required
 to link it.  Note: Signal seems to limit us to a maximum of 1 main device and
 three linked devices. After that you need to get a new number.

  * On the secondary host:
```
    systemctl stop signal-cli
    su - signal-cli -s /bin/bash
    signal-cli --config /var/lib/signal link -n $(hostname)
```
  * On the primary host (replace LINK with the link displayed on the secondary host):
```
    systemctl stop signal-cli
    su - signal-cli -s /bin/bash
    signal-cli --config /var/lib/signal -u +15555555555 addDevice --uri "LINK"
```
  * Back on the secondary host:
```
    signal-cli --config /var/lib/signal -u  +15555555555 receive
    exit
    systemctl start signal-cli
```
  * Back on the primary host:
```
    exit
    systemctl start signal-cli
```
  * Send a group message from your own client to ensure the new device is sync'ed.
  * Use the `alert` command to ensure it is working.

## Adding groups

The [alerter](/alerter) can send messages to signal groups.

If you want to create a new group:

 * Create the group in your own signal client, adding yourself.

 * Configure the group with a Group Link

 * Copy the group link

 * On any server (primary or secondary), request to join the group
```
systemctl stop signal-cli
su - signal-cli -s /bin/bash
signal-cli --config /var/lib/signal -u '+15555555555' joinGroup --uri URI
```

 * Send a message to the group from your own signal client (for some reason,
   signal-cli can't send a message until it has received one).

 * Check out the journald log (`journalctl -u signal-cli`) and look under Group Info
   for the id of the group (base64 encoded long string).

 * Add your group id to the `alerter` inventory variable.

## Safety Number changes

Unfortunately if a safety number changes, no messages can be sent until you
verify the safety number.

On the primary host, take these steps:

Stop signal-cli: `systemctl stop signal-cli`

Replace the number with 5's with our actual signal number, replace the number with 6's with the number that has had it's safety number change:

    su - signal-cli -s /bin/bash
    signal-cli --config /var/lib/signal-cli -u +15555555555 listIdentities -n '+16666666666'

Then, compare the safety numbers listed with the safety number you are expecting (look up the contact in Signal, and click to view the safety number.)

    signal-cli --config /var/lib/signal -u +15555555555 trust -v 'FINGERPRINT' '+16666666666'

Start signal-cli: `systemctl start signal-cli`

You only have to do this on the primary host - not again on all others.

