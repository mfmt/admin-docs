+++
date = 2020-12-06T13:53:34Z
title = "Overview of Monitoring and Alerting"
categories = ["logging", "alerts", "intrusion-detection"]
weight = -100
+++

May First uses a variety of tools that work together to provide monitoring and
alerting of our systems to warn us of impending problems or immediate outages.

There are three main jobs:

 * Detecting problems
 * Communicating problems to a central storage location
 * Notifying us of the problems

The heart of our system is our central [elasticsearch](/elasticsearch)
database.

Information is sent from each individual server to elasticsearch via:

 * [journalbeat](/journalbeat), which sends select data from journald
 * [metricbeat](/metricbeat), which sends server health data, 
 * [pigeon](/pigeon), which detects anomolies and outputs them to journald
 * [simplemonitor](/simplemonitor), installed on just one remote server, which
   tests network connections 

Admins are notified of problems via:

 * [elastalert](/elastalert), which tails the elasticsearch data and takes
   action when a condition is met (e.g. send an email)
 * [simplemonitor](/simplemonitor), which manages it's own independent alerting
   system so we get notified even if elasticsearch is down
 * [kibana](/kibana), the web interface to the elasticsearch data, which has
   dashboards to display all info, including non-urgent information that can
   help us spot problems before they become urgent.
 * [glance](/glance), a dead simple api-based web site that queries elastic
   search and provides a quick glance at whether or not we are experiencing
   problems.

## Notifications

When, how and what should admins be notified about?

Every notification should fall into one of three priorities:

 * Critical
 * Warning
 * Info

### Critical

These notifications are the most urgent and typically would be sent to cell
phones.

There is an expectation that critical warnings will be fixed within 4 hours
(and they typically re-alert every four hours).

Since they are intrusive and stress inducing, they should only be sent when
there is a real expectation that the sysadmin getting the alert will stop what
they are doing and address the problem. 

Examples warranting a critical alert:

1. When there is a problem users are noticing now or are about to notice.
 * Yes: Partition is 95% full, No: partition is 85% full
 * Yes: Connections to the web server are timing out, No: load average is getting high
2. When there is a problem that immediate action can fix
 * Yes: a service has crashed, No: there's a lot of disk i/o
 * Yes: a user account is probably compromised, No: a user is using lots of resources
3. When a problem requires immediate action
 * Yes: a production server is offline, No: a backup failed
 * Yes: the central authentication server is failing, No: a dev or staging server crashed

### Warning

Warnings are less urgent and typically would be sent by email.

Warnings should be resolved within about 24 hours (and they typically re-alert
every four hours).

A critical alert is warranted when there is an impending problem that is not critical:

 * Yes: Partition is 85% full, No: a user just hit 100% of their disk capacity
 * Yes: RAID failure or disk errors detected

### Info

Info notifications are not pushed out to administrators. Instead they are
displayed in a dashboard. Warnings are designed to give us a heads up about
something that is not a problem now, but could develop into a problem.

For example:

 * Repeated read errors in a backup
 * A service temporarily hits a max usage counter

## Intrusion detection strategy

In addition to alerts sent to humans, our alert system can also trigger
[bruce](/bruce), our IP banning software, to ban an IP.

## Who monitors the monitors?

If a monitoring service stops working, we don't get alerts. That's bad.

The following conditions are checked:

 * *elasticsearch stops running* - if elasticsearch stops running, then
   elastalert can no longer connection to it and generates an error accessible
   via `journalctl`. We have a `simplemonitor` command check the searches for
   this error and raises a critical alert if it finds it.

 * *elastalert stops running* - `simplemonitor` checks with `systemctl` to
   ensure that the elastalert services is properly running and raises a
   critical alert if it's not.

 * *simplemonitor stops running* - If the simplemonitor service dies, then
   our `pigeon` check on running services should report a problem.

 * *pigeon stops working on a given server* - `pigeon` is configured to send a
   heartbeat every ten minutes. The [glance](/glance) dashboard picks up any
   server that fails to send a heartbeat.

