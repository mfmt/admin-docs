+++
date = 2021-05-03T13:53:34Z
title = "Jitsi Meet Jigasi Server"
categories = ["jitsimeet"]
+++

## Overview 

The [jitsimeet jigasi server](https://github.com/jitsi/jigasi) provides
integration with a SIP gateway enabling call-in and dial-out features for Jitsi
Meet and also transcription services via a variety of back ends.

We are using the dial-in service with a phone number provided via the corporate
[Twilio](https://twilio.com/) service. Any Jitsi meet conference with a dial-in
participant is not encrypted and can be leaked.

We are using the transcription service via the Jitsi meet [skynet
API](https://github.com/jitsi/skynet) which uses the [Faster
Whisper](https://github.com/SYSTRAN/faster-whisper) LLM models, which are based
on the [OpenAI Whisper](https://github.com/openai/whisper) models, which is a
general purpose speech recognition model. This LLM is run on May First
infrastructure and does no leak any conversations.

Jigasi can be installed on the same server running Jitsi meet. But, the skynet API
should be installed on a dedicated server.

## Setting up Jigasi with a call-in Number 

Here are the general steps that happen when a jitsi meet meeting is setup with
support for a call in phone number.

1. The meeting is created by the meeting organizer via Jitsi Meet by simply
   starting a meeting.
1. The organizer clicks the participants icon and then clicks the button to
   invite more participants.
1. This triggers two API calls:
   * One call to the web service API that provides a list of dial in numbers
     that this Jitsi Meet instance supports (configured using the
     `dialInNumbersUrl` Jitsi Meet setting in
     `/etc/jitsi/meet/domain-config.js` file on the jisti meet server)
   * One call to the server serivce API (configured using the
     `dialInConfCodeUrl` setting) that translates a conference name for a 6
     digit number that a phone caller will enter into their key pad to be
     directed to the right conference. If this is the first time the API has
     seen a given conference name, then it generates a random 6 digit number
     and stores that number along with the conference name in a database for
     future lookups.
1. Based on the results of both API calls, the Jitsi Meet interface displays
the link to follow to join the meeting plus the call in numbers and the code to
be used for people who want to join via phone.
1. The organizer copies and pastes this info into a text message, email
message, etc to notify participants of how to join the call.  
1. If a participant dials the phone number, Twilio prompts the caller to enter
the 6 digit pin and then sends that PIN to our API service.
1. Our API service looks up the corresponding conference room for that pin and
sends an XML response back to Twilio directing Twilio to transfer the caller to
our Jibri instance with the SIP header `X-Room-Name` set to the proper
conference room.
1. Our Jibri instance connects that incoming SIP caller to the running
conference.  

### Getting a Twilio phone number

The first step in setting up jigasi is to get a Twilio phone number. The Twilio
interface may have changed, but as of May 2021, the process was:

1. Go to the Phone Numbers menu in Twilio 
2. Click Buy a Number 
3. Don't configure the number (yet) 
4. Click `Programmable Voice -> SIP domains` 
5. Click the "+" sign to add a new SIP domain.  
6. Add a Friendly Name and SIP URI 
7. Under Voice Authentication, click the "+" sign next to Credential List
and create a username and password (use your phone number as the username, 
without the 1).  
8. Under Call Control Configuration: 
   * Configure with: Webhooks, TwiML Bins, Functions, Studio, Proxy 
   * A Call Comes In: TwiML Bin, then click "+" to add a TwiML
      Bin with the content (replace phone_number and domain): 
        <?xml version="1.0" encoding="UTF-8"?> 
          <Response> 
            <Dial answerOnBridge="true"> 
              <Sip> sip:{{ phone_number }}@{{ domain }}.sip.us1.twilio.com </Sip> 
            </Dial> 
          </Response> 
9. Under SIP Registration, click to Enable 
10. Under SIP Registration Authentication, choose the same
    Credentials List you created above 
11. Save the SIP domain.  
12. Go back to the Numbers screen and open your phone number 
13. Go to the "A Call Comes in" section 
    a. Choose "TwiML Bin" 
    b. Select the same TwiML Bin you created above.

### Variables

In the server's yaml file, define the twilio sip login (e.g.
`1234567890@orgname.sip.us1.twilio.com`) as `jigasi_sip_login`.

Record the twilio password in the ansible secrets.txt vaule with the key:
`vault_jibri_sip_password` 

You will also need the following:
 * `jigasi_jitsimeet_conference_mapper_git_repo` (set to: `https://code.mayfirst.org/mfmt/jitsimeet-conference-mapper.git`)
 * `jigasi_jitsimeet_conference_mapper_version` (set to the latest commit)
 * `jigasi_dialin_numbers` (set to your Twilio number)

### The post install manual step. 

Wah. This is so annoying. The XMPP password is defined by the installer, but
frustratingly not provided in `jigasi/sip-communicator.properties`. To make
matters more ridiculous, it's defined in this file base64 encoded, but in the
spot we need it, it has to be plain text. So there's no way we can complete
this automatically. Sheesh.

Here are the manual instructions: 

1. Edit the file: /etc/jitsi/jigasi/sip-communicator.properties 
2. Find the setting: net.java.sip.communicator.impl.protocol.sip.acc1403273890647.PASSWORD 
3. Echo the value to the `base64 -d` command to decode the password 
4. Find the entry: org.jitsi.jigasi.xmpp.acc.PASS= 
5. Uncomment it and set it to the decoded value of the XMPP password.

### The rest

The rest of the ansible role is dedicated to setting up our Jitsi Meet
Conference mapper API. This project is managed in it's own [git
repo](https://code.mayfirst.org/mfmt/jitsimeet-conference-mapper) with Nginx
configuration, systemd start up and letsencrypt support.

## Setting up Jigasi with transcription service

When setup with transcription, the Jigasi service, which runs on the same
server as jitsimeet, connects to a separate server running skynet.

[skynet](https://github.com/jitsi/skynet) is the Jitsi Meet generic API for
providing LLM services. And, at the moment, it uses [Faster
Whisper](https://github.com/SYSTRAN/faster-whisper), which is a faster variant
of the [Open AI Whisper](https://github.com/openai/whisper) model for
converting audio to text. No data is shared outside our network.  The details
are in the ansible role.
