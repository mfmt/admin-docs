+++
date = 2020-03-20T13:53:34Z
title = "Openssh and authentication"
categories = ["ssh"]
+++

Openssh is used to provide shell access to all servers.

We have several different needs for authentication:

## Our server authenticates regular members 

We will continue providing both password-based and ssh public key based
authentication.

## Regular members authenticate our server

Our user facing shell page will explain how all users will only ssh/sftp
into one server with one domain name: `shell.mayfirst.org` (the server will
mount all appropriate directories over sshfs).

Therefore, our members will only need to verify one fingerprint.

We will publish the fingerprint widely in our documents and password reset
email messages and also via the sshfp DNS record.

Most members will probably use trust on first use.

## Our server authenticates admin users

We will use ansible to maintain a list of ssh public key fingerprints that
provide root on all servers, and allow one-off ssh public keys on particular
servers.

Admins wishing to revoke their privileges will need to request to have their
key removed.

## Admin users authenticate our servers

We will use a combination of sshfp DNS records and trust on first use.

All admins should add the following to their ~/.ssh/config file:

    VerifyHostKeyDNS yes

When ssh'ing into a server that has never been verified, you will get a message like this:

    The authenticity of host 'mysql001.mayfirst.org (216.66.22.35)' can't be established.  
    RSA key fingerprint is SHA256:cpaHZ3ttaAS8VOhkXubLenzUJlJiE4ZNKKoK4qxrBCk.  
    Matching host key fingerprint found in DNS.

The last line indicates that the fingerprint matches the published DNS fingerprint.

Once you access this message, you will not be prompted again unless the key
changes.

## Some servers (backup servers, utility servers) authenticate system users

We have many services that require a server root or non-privileged user to ssh
into another server as a non-privileged user. 

We will use ansible to manage these ssh public keys. Every host ansible file
contains a variable with the root user's ssh public key (which must be manually
populated every time a new server is added).

## System users must authenticate utility servers

The mayfirst role maintains an `/etc/ssh/ssh_known_hosts` file on all servers
which is populated from a list stored in the `hosts.yml` file. Any server that
receives ssh connections from other servers should have it's fingerprints added
here to it can be accessed without an ssh fingerprint prompt.
