+++
date = 2020-01-23T13:53:34Z
title = "Intrusion Detection"
categories = ["logging", "intrusion-detection"]
+++

Our intrusion detection system is comprised of several components that work together:

 * [journalbeat](/journalbeat) and filebeat are programs that send logging
   files from every server in our network to our
   [elasticsearch](/elasticsearch) server.

 * [elasticsearch](/elasticsearch) is a central database that consumes and
   indexes all the logs from all our servers.

 * [bruce](/bruce) specifically queries the journalbeat information in
   elasticsearch database for IP addresses engaging in suspicious behavior and
   sends them to the [bruce-server](/bruce) to be distributed to all servers
   and banned via nft.

By centralizing our logs in one location and querying one source, we can more
effectively see a global picture of our network.
