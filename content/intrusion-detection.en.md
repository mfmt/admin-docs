+++
date = 2020-01-23T13:53:34Z
title = "Intrusion Detection"
categories = ["logging", "intrusion-detection"]
+++

Our intrusion detection system is comprised of several components that work together:

 * [journalbeat](/journalbeat) and filebeat are programs that send logging
   files from every server in our network to our
   [elasticsearch](/elasticsearch) server.

 * [elastticsearch](/elasticsearch) is a central database that consumes and
   indexes all the logs from all our servers.

 * [elastalert](/elastalert) constantly monitors the elasticsearch database
   using complex queries to find suspicious patterns and either:

   * alerts us via email/sms/etc for urgent events
   * bans IP addresses engaging in nefarious behavior via [bruce](/bruce)

By centralizing our logs in one location and querying one source, we can more
effectively see a global picture of our network.
