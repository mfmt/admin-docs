+++
date = 2020-12-12T13:53:34Z
title = "Welcome"
categories = ["orientation"]
weight = -100
+++

Welcome to the [May First Movement Technology](https://mayfirst.coop/)
administrators documentation. 

This site is intended for technical people who want to learn how the new May
First infrastructure works. As of December 2020, the new infrastructure and
this documentation is still a work in progress.

Want to get involved with the Infrastructure and Services team? See our [get
involved page](/get-involved).

Want to dive into the docs and learn how the new infrastructure works? The
[orientation page](/orientation) is a great place to start. 
