+++
date = 2024-02-23T13:53:34Z
title = "How to respond to alerts"
categories = ["alerts"]
+++

All alerts should come with a link to this page, which explains how to fix the problem.

## elasticsearch health {#elasticsearch_health}

The `elasticsearch_health` alert happens when accessing
`https://log.mayfirst.org/_cat/health` does not return a 200 http response
code. That might mean our elasticsearch database is under heavy load, is
restarting or has failed. Login to log001.mayfirst.org and check
`/var/log/elasticsearch/elasticsearch.log` and `journalctl -u elasticsearch`.

## missing pigeon heartbeat {#missing_pigeon_heartbeat}

Every host running [pigeon](/pigeon) should send a heartbeat message once a
minute. If this alert happens, it means we missed a heartbeat from the
specified host. That might be because `pigeon` has crashed (try logging into
the host and checking `systemctl status pigeon`). It might mean that our list of
hosts to check is incorrect (the generated list in is monitor002 in
`/etc/simplemonitor/hosts.yml`).

## pigeon warning {#pigeon_warning}

A pigeon warning alert should come with additional information describing what
triggered it. 

### Backup failed.

### Disk space 

### filter check

[Filter Check](/filter-check) notifies us if email sent via our servers is
landing in the spam box of the three major providers - Google, Microsoft or
Yahoo.

filter-check can be run manually. If we get an alert, for example, that a
message sent to gmail via the IP address 1.2.3.4 laned in the spambox, you can
easily repeat to see if it is still happening with:

    filter-check --sendto gmail --sendvia 1.2.3.4 --sleep 30 --headers

It will send a message, sleep for 30 seconds, and then try to retrieve the message.

If the message lands in the inbox, then either the alert was an anomoly and can
be ignore, or it was based on the email from address, subject or body. The
[pigeon](/pigeon) configuration rotates these values randomly - so they may
need to be changed.

If the message consistently ends up in the spam box, you can examine the
headers to try to figure out why.  And, if all else fails, you can remove the
IP from the filtered/bulk/priority domain name it is assigned to in order to
remove it from the pool.

## pigeon critical {#pigeon_critical}

A pigeon critical alert should come with additional information describing what
triggered it.

### certbot service failure

A common error is the certbot service failure error via key001.

1. Login to `key001`
1. Check the `/var/log/letsencrypt/letsencrypt.log` file to see which site failed.
1. You may see reference to: `/etc/letsencrypt/live/site352220/fullchain.pem`.
1. Check the renewal file: `/etc/letsencrypt/renewal/site352220.conf`
1. Ensure there is a `webroot_path = /var/www/html,` and there is not a `[[webroot_map]]` section (adjust if necessary, deleting the lines under `[[webroot_map]]` as well as the heading if necessary.
1. Manually run `certbot renew`. If it succeeds, restart the service: `systemctl restart certbot`. If it fails...
1. Check which domain name is failing, lookup in control panel, remove that domain from the web configuration.

## dovecot process limit {#dovecot_process_limit}

A dovecot process limit warning means we have seen a message like the following
in the logs:

> dovecot: master: Warning: service(imap-login): process_limit (100) reached, client connections are being dropped

This warning indicates that the dovecot server has too many connections.

Pay close attention to the server that is reporting the error, it might be a
`mailcf` server - which are the servers that respond to requests to
`mail.mayfirst.org` and proxy to the actual server, or it could be a
`mailstore` server.

If we are reaching the limit due to normal usage, you can raise the limit by
`grep`ing for 'process_limit' in the dovecot ansible role.

If we seem to be under attack, ensure that `bruce-server.service` and
`bruce-elastic.service` are properly running on `monitor002` (you may need to
restart them).

## postfix process limit {#postfix_process_limit}

A postfix process limit warning means we have a message like the following
in the logs:

> warning: service "submission" (587) has reached its process limit "100": new clients may experience noticeable delays

This is a likely sign that we are experiencing a dictionary attack against our `mailcf` servers.

Ensure that `bruce-server.service` and `bruce-elastic.service` are properly
running on `monitor002` (you may need to restart them). Or, sometimes we just
can't keep with the dictionary attack and we might consider raising the limit.

You can search Elastic search for the message "SASL LOGIN authentication
failed" to see the failures.

## MisterT Audit Relayers

If a email user has an unusually high number of bounces, we may get an alert along these lines:

>  monitor002-mistert_audit_relayers-critical: Possible compromised email relayers: sender: USER DOMAIN.ORG, connections: 361, ips: 13, failure rate: 5, messages: 712, sent: 675, bounced: 32, deferred: 5, expired: 0, spam virus: 0 0

The USER and DOMAIN.ORG will be replaced with the actual user and possibly the domain (notice that the @ sign is absent). Sometimes it will just be USER and not USER DOMAIN.ORG.

Some signs that the account is compromised include:

 * A high number of ip addresses
 * A high number of messages sent

Sometimes, however, it's a legit message that just had a lot of bounces.

You can review the messages sent by logging into monitor002 and running:

`mistert --since 1h list --sender USER@DOMAIN.ORG --list-recipients | less -R`

If you are not sure if this is spam, you can find the queue id of any deferred messages. Then, lookup the queue id in kibana and find out which host and which postfix instance sent it.

Then, log in to the host and run:

`postcat -c /etc/POSTFIX-INSTANCE -q QUEUEID | less`

If it is spam, then:
 
 * Change the user account password
 * On each mailcf server, run `systemctl restart saslauthd.service`
 * On each mail relay server, run `mf-mailq-delete SENDER`
 * Notify the member
 * Confirm it has stopped with a kibana search: `message: "sasl_username=USER@DOMAIN.ORG"`
 
## nginx worker connections {#nginx_worker_connections}

Too many worker connections.

## nginx open_files {#nginx_open_files}

Too many open files.

