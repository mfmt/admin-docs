+++
date = 2020-09-21T13:53:34Z
title = "Transition a metal server from puppet to ansible"
categories = ["ansible"]
+++

The metal servers will be the first ones we convert from puppet to ansible.
Here are the steps to complete the transition.

## Prepare server in ansible

Add the server to the `hosts.yml` file and add a
`host_vars/SERVER.mayfirst.org.yml` file. Use an existing metal server as a
template.

Don't add any of the guests yet.

## Upgrade to buster

First, follow the basic steps to upgrade to buster:

1. `rm /etc/apt/sources.list.d/*`
1. `rm /etc/apt/preferences.d/*`
1. `sed -i 's/stretch/buster/' /etc/apt/sources.list`
1. `apt update && apt dist-upgrade`

When prompted, chose 'Y' to overwrite all configuration files with their
defaults. You can choose to not restart any services or just avoid restarted
the guest kvm instances.

Lastly: `apt autoremove` and `apt purge $(debortphan)`

## Update kvm configuration.

All kvm guests with a scsi disk must have their `/etc/kvm-manager/<GUEST>/env`
file edited and `HDB_DRIVER=scsi` should be changed to `HDB_DRIVER=scsi-hd`.

## Apply ansible

Run `./seed-playbook-live --secrets SERVER.mayfirst.org`

You may need to manually stop and start unbound via `systemctl`.

## Get rid of no longer needed puppet packages

`apt purge monkeysphere puppet agent-transfer augeas-lenses backupninja bind9-host certbot courier-authlib debconf-utils distro-info-data dnsutils emacs-common exim4-base facter fonts-lato fuse geoip-database gnupg-agent gnutls-bin groff-base hiera iamerican ibritish ienglish-common install-info ispell javascript-common laptop-detect maildrop  multiarch-support munin-common perl-modules-5.24 pwgen python-acme rake rdiff-backup ruby runit runit-helper runit-systemd s-nail sgml-base sysuser-helper task-english trickle wamerican emacs-gtk emacs-lucid emacs-nox`

Then...

```
rm -rf /etc/monkeysphere
rm -rf /var/lib/monkeysphere
rm -rf /etc/backup.d
rm -rf /etc/munin
rm -rf /etc/sv
rm -rf /var/cache/puppet
rm -rf /etc/puppet
rm -rf /etc/network/interfaces.d/*
rm -rf /etc/udev/rules.d/10-persistent-net*
rm /etc/systemd/system/ssh-agent-root.service
```

Followed by `apt autoremove` (make sure cryptsetup packages are not removed).
