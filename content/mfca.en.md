+++
date = 2023-11-19T03:53:34Z
title = "May First Cerificate Authority"
categories = ["x509"]
+++

While May First mostly depends on Lets Encrypt for our [PKI
infrastructure](/keyserver), we do operate our own certificatte authority for
signing certificates in our dev and/or testing environment and for internal
only connections.

The certificate authority key is saved in our ansible vault using the variable
name `vault_mfca_key` and the signed certificate key is stored in the variable
`mayfirst_mfca_cert`.

The certificate is saved to the directory
`/usr/local/share/ca-certificates/mfca.crt`, which is picked up by the
`ca-certificates` debian package and incorporated into the `/etc/ssl/certs`
directory so all local applications will trust certificates signed by our key.

The certificate is generally signed for 3 years which means it needs to be
periodically renewed.

To renew it:

1. Extract the private key from the value and save to a file called `mfca.key`.
1. Ensure the old key is in your current directory and named `mfca.crt`.
1. Create a certificate signing request: `openssl x509 -x509toreq -in mfca.crt -signkey mfca.key -out new-server.csr`
1. Sign it: `openssl x509 -req -days 1024 -in new-server.csr -signkey mfca.key -out new-cacert.pem`
1. Copy new cert over old cert: `mv new-cacert.pem mfca.crt`
1. Copy contents of `mfca.crt` to the `mayfirst_mfca_cert` variable in `hosts.yml`
1. Optioanlly copy `mfca.crt` to your `/usr/local/share/ca-certificates/mfca.crt` and run `sudo dpkg-reconfigure ca-certificates` and import it into your browser.
