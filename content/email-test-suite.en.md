+++
date = 2022-10-02T13:53:34Z
title = "Email Test Suite"
categories = ["mail" ]
+++

Since we use round robin DNS for our client facing servers (`mailcf`) and we
have multiple MX servers that can accept email, it is hard test each specific
server to ensure it is working properly.

The [May First Email Test
Suite](https://code.mayfirst.org/mfmt/mayfirst-email-test-suite) is a simple
script that can iterate through our list of servers and ensure they are all
working.

The test suite expects to be run from  your local workstation and be given a
username, password and associated email address capable of sending and
receiving email via our network.

With this information, it will:

 * Test all mailcf servers to ensure:
   * They have a properly configured x509 certificate
   * They all can relay email from authenticated users
   * They cannot relay email from users with incorrect passwords
   * They can properly proxy an IMAP connection via port 993 (tls) and port 143 (starttls)
   * They can find email sent earlier
 * Test all mx servers to ensure:
   * They can accept and deliver email configured to use our network
   * They refuse email not configured to use our network.
