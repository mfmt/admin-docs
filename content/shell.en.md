+++
date = 2020-03-20T13:53:34Z
title = "SSH and Secure FTP access to web sites"
categories = ["ssh"]
+++

## Goals

The goals of our ssh/sftp access configuration are:

 * Provide one address for all members to ssh into (e.g. `shell.mayfirst.org`)
 * Users do not need to know which weborigin server their web sites are on,
   they are automatically given access to the right server when the ssh into
   the shell server.
 * Ability to use ssh or sftp

## High level overview of how it works

When a user ssh's into `shell.mayfirst.org`, a `ForceCommand` configuration in
`sshd_config` automatically ssh's them into their configured origin server or
logs them into the shell server while mounting their weborigin servers
directories via sshfs.

The `ForceCommand` is a shell script - `a-ssh-wrapper` - which detects whether the user
is connecting via ssh or sftp and calls the appropriate commands.

## The details

When a "Server Access" record is added to the control panel, the following
actions are triggered:

 * A user account is created on shell.mayfirst.org (populating `/etc/passwd`
   and `/etc/shadow`
 * A home directory (owned by root) is created in `/home/user/<username>`
 * A private and public key is generated in
   `/home/user/<username>/.ssh/id_rsa{,.pub}
 * The public key is copied to `/var/lib/red/pubkeys/<username>` where it is
   served via nginx via `https://shell.mayfirst.cx/<username>` (only accessible
   within our network)
 * The user's weborigin server is stored in `/var/lib/red/access/<username>`

 If the user is logged into the control panel at the time of creation or the
 user logs in after the record is created, the following actions are triggered:

 * On the weborigin server, the file `/var/lib/red/authorized/<site-id>/<username>` is
   created with a timestamp 24 hours in the future.
 * The `mf-ssh-rebuild-authorized-keys` script is run, which:
   * Finds all files in `/var/lib/red/authorized/` with a timestamp greater than now
   * For each username, the public key is fetched via `https://shell.mayfirst.cx/<username>` and copied
     to the respective site's authorized_keys file.
   * For each site without a username dated in the future, the authorized_keys file is truncated.

## Trouble shooting

To successfully ssh or sftp into the shell servers, at least two things need to happen:

1. The control panel "Server access" record must be in place. This grants the
   user shell access on the `shell001` server.
2. They must login to the Control panel *as the shell user* within 24 hours of
   attempting to ssh into the shell server. This step grants the shell server
   access to the weborigin server.

When trouble shooting, here are some steps to take:

1. First, ensure that they can successfully log into `shell001`.
  * Double check that a Server Access record exists in the control panel and is active.
  * On shell001, run: `journalctl -u 'ssh' | grep $username` to see if they have successfully (or not successfully) connected.
2. Ensure that they have logged into the control panel within the last 24 hours.
  * Log in to the weborigin server and check the `~/.ssh/authorized_keys` file for the web conf user (e.g. site1234writer). It should contain a key.
  * When a user tries to ssh or sftp into `shell.mayfirst.org` *and access is granted by shell001*, then
the `/usr/local/bin/mf-ssh-wrapper` script is invoked. This scrit logs all errors to journald with the
syslog tag of the username. Search for errors with: `journalctl -t $username`.
  * Add your ssh public key to the user's Serve Access record and try it yourself. Be sure to try SFTP if the problem is with SFTP and ssh if the problem is with ssh.
  * Become the server access user on shell001 and execute `mf-ssh-wrapper`. Run by itself it will mimic ssh access and
    should drop you into a shell on the weborigin server.


