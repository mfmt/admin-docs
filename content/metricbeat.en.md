+++
date = 2019-09-09T13:53:34Z
title = "Metricbeat"
categories = ["logging"]
+++

## General Info
[Metricbeat](https://www.elastic.co/guide/en/beats/metricbeat/current/index.html)
sends selected server health data to our [elasticsearch](elasticsearch) server.

All servers (by default, not a requirement) run metricbeat with the default
system module enabled. All servers share a metricbeat username and password
authorizing them to connect to the es server.

## Index Lifetime Management

Index Lifetime Management (ILM) is how elasticsearch manages log rotation and
retention. It's covered in it's own [ILM page](/elasticsearch-index-lifetime-management).
