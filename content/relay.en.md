+++
date = 2022-04-12T13:53:34Z
title = "Relay mail servers configuration"
categories = ["mail"]
+++

As documented on our [postfix page](/postfix), relay servers send email out to
the world. This page provides additional information about how they are configured.

## Domain Names and IP addresses

Since these servers rely on dozens of IP addresses to send email in
parallel, adding and removing IP addresses to the configuration files is an
on-going task.

Unlike most IP addresses, special DNS records are required for our outgoing
relay IPs.

1. Do *not* assign the IP address to the host - that will interfere with our
   ability to ssh into the server if we temporarily disable the IP address on
   the server.

2. **A record to the relay type domain**: Assign an A record linking the IP
   address to the domain name representing it's type, e.g.
   `bulk.relay.mayfirst.org`, `priority.relay.mayfirst.org`, or
   `filtered.relay.mayfirst.org`. This allows us to send email to the relay
   type and, using round robin DNS, get a long list of IP addresses that will
   be randomly used in a way that spreads the load.

2. **PTR record to the relay type domain**: Each IP must also have a
   corresponding PTR record back to the relay type domain.

    Why? When postfix contacts the recipient mail server to deliver email, it
    identifies itself with it's relay type domain.

    The recipient mail system typically checks the IP address, uses a reverse
    DNS lookup to confirm it points to the domain name that the sending server
    identifies as, and lastly does a direct lookup of the domain name to see if
    it also corresponds to the IP address. If any of these checks fail, the
    server may reject the email.

3. **A record to spf.mayfirst.org**: You must assign the IP address to our SPF
   record to alert the world that this IP address is allowed to relay email on behalf
   of any domain that specified `a:spf.mayfirst.org` for their domain name.

## Repeat bouncers

Protecting the reputation of our outgoing relay servers is critical to ensuring
good deliverability rates.

One way our reputation is damanged is by sending a lot of email messages to
email addresses that don't exist.

Using [Mr. T](/mistert), we can generate a list of frequent bouncers and refuse
to accept email with a known-to-bounce recipient before it even enters our
queue. 

By pre-bouncing these records, we can reduce the number of message attempts to
addresses we know will bounce.

See our [Mr. T page](/mistert) for more details.

## Are we landing in the spam box?

Via [pigeon](/pigeon), we regularly use [filter-check](/filter-check) to send
messages to the ~~three major providers~~ Yahoo and see if we are landing in the spam box
or the inbox. Gmail and Microsoft seem to send email to the spam box for unknown reasons.

While this doesn't demontrate that all messages should land in the inbox, it
does provide us with an alert if suddenly messages start going to spam.

## DKIM Signing

The relay mail servers pass all email through
[opendkim](http://www.opendkim.org/), which adds a digital signature to the
email header. 

More info on how DKIM works is available on our [dkim page](/dkim).

## Warming up an IP address

To warm up a new IP address, you can edit the inventory file for the given
mail relay record, for example:

```
  - name: filtered006
    ip: 204.19.241.2
    type: filtered
    default_destination_recipient_limit: 50
    default_destination_rate_delay: 30m
```

The `default_destination_recipient_limit` and `default_destination_rate_delay`
can be added. By setting `default_destination_recipient_limit` to a number
above one it ensures that the `default_destination_rate_delay` will apply to
all messages sent to a given domain, not to a given email address.

Messages will start collecting in the active queue for email that exceeds the limit.

Run: `mf-mailq-report` to see how many are collecting.

Then, run `mf-postfix-drain-queues from-postfix-instance "to-postfix-instance1 to-postfix-instance2 etc"`

In other words, if `postfix-bulk001`, `postfix-bulk002`, and `postfix-bulk003` are already warmed up, and you want
to empty the active queue for `postfix-bulk010`, you would run:

`mf-postfix-drain-queues postfix-bulk010 "postfix-bulk001 postfix-bulk002 postfix-bulk003"`

All active messages in `postfix-bulk010` will be evenly distributed between the
three warmed up instances.

