+++
date = 2023-03-22T13:53:34Z
title = "Disk Management"
categories = ["disk-management"]
weight = -100
+++

When a disk on a virtual guest starts to fill up, we have two options:

 * Allocate a new disk: if the disk in question is a data disk serving member
   data on a web server or a mail server, we generally want to allocate a new
   disk so our data disks don't grow too large, making them more difficult to
   backup or to run disk utilities on. The web and mail servers use symlinks
   from the /home directory, allowing us to mount as many separate disks in the
   /media directory as needed.
 * Extend the disk: other times it makes more sense to simply extend the disk
   that is filling up to relieve the pressure.

Either way, the approach is similar:

 * In our seed inventory repository, edit the virtual guest in need of more
   disk space. Either add a new disk definition or increase the size of the
   existing one. When adding a new one, the name of the data disk should be in
   the format `dataNNNN` and be unique across all servers (when running the 
   `sower playbook` command, the inventory will be validated to ensure no duplicate
   data disks are defined). When specifying units, always use either "g" or "t".
 * Use `sower playbook` to push the playbook to the virtual server's host server. You can speed
   things up by passing the lvm-file tag with `--tags lvm-file`. Ansible will update
   the server's configuration file tracking all logical volumes and it will automatically
   call the `mf-manage-logical-volumes` command that will adjust all logical volumes for you.
 * Restart the guest. For changes to be seen on the guest, you must restart the guest.
