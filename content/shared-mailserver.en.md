+++
date = 2023-12-11T13:53:34Z
title = "Setting up a new shared mail server"
categories = ["mail", "red", "control-panel"]
+++

The following steps should be taken to setup a new shared mail server:

1. In ansible seed repo, edit `hosts.yml` to copy each line with `mailstore002` to
   your new name.
1. Execute `sower --live generate:mailstore --parent hostxxx.mayfirst.org` to generate a new mailstore yaml file.
1. Follow [regular kvm guest setup steps](/kvm-manager/#what-to-do-specifically)
1. Be sure you created the ssh key in the step above!
1. Insert the server into the `red_server` table:
   `INSERT INTO red_server SET server = 'mailstore00x.mayfirst.org', mountpoint = 'data0xxx', server_dedicated_mail = 1;`
