+++
date = 2020-03-13T13:53:34Z
title = "Setting up console access (cereal and IPMI) to servers in our cabinet"
categories = ["console"]
+++

We use the [cereal](http://cmrg.fifthhorseman.net/wiki/cereal) package to
manage console access to our physical servers with hardware serial ports and
the ipmiutils package to provide serial access for those that depend on IPMI.

In both cases, the setup is automatically handled by ansible via the console variable.

## Physical serial port

Here's an example of setting up a hardware serial server:

    console:
      host: clr.mayfirst.org
      name: wiwa
      tty: /dev/ttyUSB29
      id: FTDI_FT232R_USB_UART_ST161546

The host key indicates which serial console server it should be setup on. The
name indicates that the cereal session will be named "wiwa" and the username
providing access to it will be "wiwa-console". The `tty` is the path to the
device connected to the host server's serial port. The `id` key is currently
not used but may be used in the future to ensure the kernel doesn't change
device paths (you can find it via `ls -l /dev/serial/by-id/`).

If you plug in a new device and you are not sure which console port it is
using, here is a shell snippet that will iterate over all of them. It will use
the `test-console` user account, creating and destroying each one as you go.

First, login as `test-console@console.mayfirst.org`

Then, run the script as `root`. When it pauses, asking you if you want to kill
an port, type...

    cereal attach test

...from the `test-console` console and hit enter a few times to see if you get
your server.

    for i in $(seq 1 32); do \
      read -p "setup: $i" && \
      cereal-admin create test /dev/ttyUSB$i 115200 test-console test-console && \
      sleep 4 && \
      cereal-admin start test && \
      read -p "kill it $i " && \
      cereal-admin stop test && \
      cereal-admin destroy test; \
    done

## IMPI serial port

Here's an example of an IPMI setup:

    console:                                                                               
        host: clr.mayfirst.org                                                               
        name: ali                                                                            
        ipmi_address: 192.168.56.4

With IPMI you must specify an IP address.

To see how to configure the bios of a new server to use ipmi, see [ipmi](/ipmi).
