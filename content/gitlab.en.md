+++
date = 2020-12-17T13:53:34Z
title = "GitLab"
categories = ["devops"]
+++

We will be using GitLab mainly as a web-based Git-repository manager and issue tracker but this powerful open source DevOps tool integrates other functions we hope to explore such as continuous integration and deployment pipelines.

We use ansible to deploy GitLab on a kvm virtual machine. Our playbook adds the Omnibus GitLab apt and repositories, installs the gitlab-ce package, and makes some basic changes to the Gitlab configuration file.

Our plan is to use GitLab as the central hub for participating in the development of MFMT technology insfrastructure and services.