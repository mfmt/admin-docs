# Documentation

If you want to see the documentation formatted, install hugo (https://gohugo.io/) and then,
in this directory, run:

`hugo server`

Here are some initial guidelines.

## Audience and naming

This repo is for docs for administrators. There will be a separate repo for
user facing docs.

Use the shortest, simplest name for the file.

Use the names of the programs being described, e.g. roundcube.en.md and
horde.en.md, for pages that describe how these programs are setup and
configured.

When using multi-word names start from the general and move the specific rather
then trying to create a sentence. So, we will have 'ansible-hacking' rather
then `hacking-ansible` and 'ansible-docker-initialize' rather then
ansible-initialize-docker`.

## Language

All english files should end with en.md, spanish files with es.md. Files don't
have to be translated into Spanish as we go.

## Taxonomy

Let's start with one simple taxonomy:

 * categories (add terms as necessary)
   * email
   * email lists
   * web
   * dns
   * database
   * ansible
   * logging
   * alerts

## hugo front matter

Please use simple hugo style front matter:

+++
date = 2015-10-01T13:53:34Z
title = "Postfix"
categories = ["mail"]
+++

## Conventions for referencing servers and URLs

The goal is for the documentation to be useful both to help while in dev mode
and also to be used, as is, when we go live.

That makes it hard to refer to URLs and server names. For example, in dev mode,
the default URL is: mayfirst.dev. But in live mode, it will be mayfirst.org.
So, instead of writing those URLs, let's write the variable: `{{<domain>}}`,
e.g. "Connect to kibana via: https://monitor.{{<domain>}}:5601/"

Also, in our dev environment, all servers are named sequentially for simplicity
(e.g. nscache001, nscache002), however, in production they could be anything
So, when referring to a particular server, let's write nscachexxx to mean
whichever nscache server is relevant, e.g. "SSH into
root@nscachexxx.{{<domain>}}.
